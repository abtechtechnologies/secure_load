<?php
class Transactiontotal extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM transaction_total WHERE deleted = 0;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['transaction_number']] = $row['cost'];
		}
		
		return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM transaction_total WHERE user_id = ' . $id . ' AND deleted = 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function writeData($transaction_number, $cost)
	{
		$sql =
			"INSERT INTO `transaction_total`
			(
				`created_by`, 
				`transaction_number`, 
				`cost`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . $transaction_number . "', 
				'" . $cost . "'
			);";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `content` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}