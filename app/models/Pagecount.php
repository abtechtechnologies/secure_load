<?php
class Pagecount extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function increaseCount()
	{
		$db = $this->DB->query('SELECT count FROM page_count WHERE id = 1 AND deleted = 0');
				
		foreach ($db->result_array() as $row) {
			$count = $row['count'];
		}
		
		$db = $this->DB->query("UPDATE page_count SET count = '" . ($count + 1) . "' WHERE id = 1;");
	
		return $db;
	}
	
	/**
	 * Get the number of visits for a given page_id
	 * 
	 * @param int $page_count_id     1 => home page; only option for now
	 * @return int
	 */
	public function getCount($page_count_id)
	{
		$sql = 'SELECT count FROM page_count WHERE id = ' . $page_count_id . ' AND deleted = 0';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return = $row['count'];
		}
	
		return $return;
	}
}