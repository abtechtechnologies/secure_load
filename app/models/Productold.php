<?php
class Productold extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$return = false;
		
		if (is_numeric($id)) {
			$sql = 'SELECT * FROM product_old WHERE id = ' . $id . ' AND deleted = 0;';
			
			$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
			$return = false;
			
			foreach ($db->result_array() as $row) {
				$return = $row;
			}
		}
		
		return $return;
	}
	
	public function getRecordAdmin($id)
	{
		$return = false;
		
		if (is_numeric($id)) {
			$sql = 'SELECT * FROM product_old WHERE id = ' . $id . ';';
			
			$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
			$return = false;
			
			foreach ($db->result_array() as $row) {
				$return = $row;
			}
		}
		
		return $return;
	}
	
	public function getRecentId()
	{
		$sql = 'SELECT * FROM product_old WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['parent_id'];
		}
		
		return $return;
	}
	
	public function getRecords($include_children = false)
	{

		$sql = 'SELECT * FROM product_old WHERE deleted = 0 ORDER BY id desc;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductsByProductTypeId()
	{
		
		$sql = 'SELECT * FROM product_old WHERE deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['product_old_type_id']])) {
				$return[$row['product_old_type_id']] = array();
			}
			
			$return[$row['product_old_type_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductsByVendorId($vendor_id)
	{
		
		$sql = 'SELECT * FROM product_old WHERE deleted = 0 AND parent_id = 0 AND vendor_id = ' . $vendor_id. ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM product_old WHERE 1;';
		
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecentProductId()
	{
		$sql = 'SELECT * FROM product_old WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getProductsByProductType($product_old_type_id)
	{
		$sql = 'SELECT * FROM product_old WHERE deleted = 0 AND parent_id = 0 AND product_old_type_id = ' . $product_old_type_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductsByParentId($product_old_id)
	{
		$sql = 'SELECT * FROM product_old WHERE deleted = 0 AND parent_id = ' . $product_old_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductsByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row['product_old_id'];
		}
		
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM product_old WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductSizes()
	{
		$sql = 'SELECT * FROM product_old WHERE deleted = 0 AND parent_id > 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($data)
	{
		
		if ($data['product_old_id'] > 0) {
			$sql =
			"UPDATE `product_old` SET
				modified_by = " . $_SESSION['user_id'] . ",
				name = '" . str_replace("'", "\'", trim($data['name'])) . "',
				description = '" . str_replace("'", "\'", trim($data['description'])) . "',
				product_old_type_id = '" . str_replace("'", "\'", trim($data['product_old_type_id'])) . "',
				unit_name = '" . str_replace("'", "\'", trim($data['unit_name'])) . "',
				grower_id = '" . str_replace("'", "\'", trim($data['grower_id'])) . "',
				price = 0,
				has_child = 1,
				parent_id = 0,
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['product_old_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `product_old`
			(
				`created_by`,
				`name`,
				`description`,
				`product_old_type_id`,
				`price`,
				`has_child`,
				`parent_id`,
				`unit_name`,
				`grower_id`,
				`attachment_file_name`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", trim($data['name'])) . "',
				'" . str_replace("'", "\'", trim($data['description'])) . "',
				'" . str_replace("'", "\'", trim($data['product_old_type_id'])) . "',
				'0',
				'1',
				'0',
				'" . str_replace("'", "\'", trim($data['unit_name'])) . "',
				'" . str_replace("'", "\'", trim($data['grower_id'])) . "',
				'" . str_replace("'", "\'", trim($data['attachment'])) . "'
			);";
		}
		
		$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		//Create/Update sub-product_olds
		if ($status == 1) {
			//Existing Product
			if ($data['product_old_id'] > 0) {
				$sub_product_olds = $this->getProductsByParentId($data['product_old_id']);
				$count = 1;
				
				$formatted_sub_product_olds = array();
				
				foreach($sub_product_olds as $product_old_id => $product_old_data) {
					$formatted_sub_product_olds[$count] = $product_old_data;
					$count++;
				}
				
				for($i = 1; $i < 4; $i++) {
					if ($data['new_sub_product_old_name' . $i] != '') {
						
						if (isset($formatted_sub_product_olds[$i])) {
							//Sub product_old existed before, and the data has been changed. Do nothing otherwise
							if ($formatted_sub_product_olds[$i]['name'] != $data['new_sub_product_old_name' . $i] || $formatted_sub_product_olds[$i]['price'] != $data['new_sub_product_old_price' . $i] || $formatted_sub_product_olds[$i]['units_total'] != $data['new_sub_product_old_inventory' . $i]) {
								if ($status == 1) {
									$sql =
									"UPDATE `product_old` SET
										modified_by = " . $_SESSION['user_id'] . ",
										name = '" . str_replace("'", "\'", trim($data['new_sub_product_old_name' . $i])) . "',
										price = '" . str_replace("'", "\'", trim($data['new_sub_product_old_price' . $i])) . "',
										units_total = '" . str_replace("'", "\'", trim($data['new_sub_product_old_inventory' . $i])) . "',
										unit_name = '" . str_replace("'", "\'", trim($data['unit_name'])) . "'
									WHERE
										id = " . str_replace("'", "\'", $formatted_sub_product_olds[$i]['id']) . ";";
									
									$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
								} else {
									break;
								}
							}
							//Sub product_old did not exist prior
						} else {
							$sql =
							"INSERT INTO `product_old`
							(
								`created_by`,
								`name`,
								`price`,
								`units_total`,
								`product_old_type_id`,
								`parent_id`,
								`has_child`,
								`unit_name`
							) VALUES (
								'" . $_SESSION['user_id'] . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_name' . $i])) . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_price' . $i])) . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_inventory' . $i])) . "',
								'" . str_replace("'", "\'", trim($data['product_old_type_id'])) . "',
								'" . str_replace("'", "\'", trim($data['product_old_id'])) . "',
								'0',
								'" . str_replace("'", "\'", trim($data['unit_name'])) . "'
							);";
							
							$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
						}
						//Sub product_old field is empty, delete if something existed prior
					} else {
						if (isset($formatted_sub_product_olds[$i])) {
							$sql =
							"UPDATE `product_old` SET
							modified_by = " . $_SESSION['user_id'] . ",
							deleted = 1
						WHERE
							id = " . str_replace("'", "\'", $formatted_sub_product_olds[$i]['id']) . ";";
							
							$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
						}
					}
					
					$count++;
				}
				//New Product
			} else {
				$parent_id = $this->getRecentProductId();
				
				$this->load->model('inventory');
				$status = $this->inventory->writeData(0, $data['initial_inventory'], $parent_id);
				
				if ($status == 1) {
					if ($data['new_sub_product_old_name1'] != '' && $data['new_sub_product_old_price1'] != '' && $data['new_sub_product_old_inventory1'] != '') {
						$sql =
						"INSERT INTO `product_old`
							(
								`created_by`,
								`name`,
								`price`,
								`units_total`,
								`product_old_type_id`,
								`parent_id`,
								`has_child`,
								`unit_name`
							) VALUES (
								'" . $_SESSION['user_id'] . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_name1'])) . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_price1'])) . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_inventory1'])) . "',
								'" . str_replace("'", "\'", trim($data['product_old_type_id'])) . "',
								'" . str_replace("'", "\'", trim($parent_id)) . "',
								'0',
								'" . str_replace("'", "\'", trim($data['unit_name'])) . "'
							);";
						
						$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
					}
					
					if ($data['new_sub_product_old_name2'] != '' && $data['new_sub_product_old_price2'] != '' && $data['new_sub_product_old_inventory2'] != '') {
						$sql =
						"INSERT INTO `product_old`
							(
								`created_by`,
								`name`,
								`price`,
								`units_total`,
								`product_old_type_id`,
								`parent_id`,
								`has_child`,
								`unit_name`
							) VALUES (
								'" . $_SESSION['user_id'] . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_name2'])) . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_price2'])) . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_inventory2'])) . "',
								'" . str_replace("'", "\'", trim($data['product_old_type_id'])) . "',
								'" . str_replace("'", "\'", trim($parent_id)) . "',
								'0',
								'" . str_replace("'", "\'", trim($data['unit_name'])) . "'
							);";
						
						$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
					}
					
					if ($data['new_sub_product_old_name3'] != '' && $data['new_sub_product_old_price3'] != '' && $data['new_sub_product_old_inventory3'] != '') {
						$sql =
						"INSERT INTO `product_old`
							(
								`created_by`,
								`name`,
								`price`,
								`units_total`,
								`product_old_type_id`,
								`parent_id`,
								`has_child`,
								`unit_name`
							) VALUES (
								'" . $_SESSION['user_id'] . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_name3'])) . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_price3'])) . "',
								'" . str_replace("'", "\'", trim($data['new_sub_product_old_inventory3'])) . "',
								'" . str_replace("'", "\'", trim($data['product_old_type_id'])) . "',
								'" . str_replace("'", "\'", trim($parent_id)) . "',
								'0',
								'" . str_replace("'", "\'", trim($data['unit_name'])) . "'
							);";
						
						$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
					}
				}
			}
		}
		
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `product_old` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		if ($status == 1) {
			$sql = 'UPDATE `inventory` SET deleted = 1 WHERE product_old_id = ' . $id;
			
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
		
		return $status;
	}
	
	public function updateCache($id, $file)
	{
		$sql = 'UPDATE `product_old` SET thumb = \'' . $file . '\' WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
}