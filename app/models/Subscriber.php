<?php
class Subscriber extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM vendor WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords($include_blank = false)
	{				
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		if ($include_blank == true) {
			$return[0] = array();
			$return[0]['name'] = 'None';
			
		}
				
		foreach ($db->result_array() as $row) {
			
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM vendor WHERE 1;';
	
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecentvendorId()
	{
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getvendorsByvendorType($vendor_type_id)
	{
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 AND parent_id = 0 AND vendor_type_id = ' . $vendor_type_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getvendorsByParentId($vendor_id)
	{
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 AND parent_id = ' . $vendor_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getvendorsByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[] = $row['vendor_id'];
		}
	
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM vendor WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getvendorSizes()
	{
		$sql = 'SELECT * FROM vendor WHERE deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getUserByEmail($email)
	{
		$sql = "SELECT * FROM subscriber WHERE deleted = 0 AND email = '" . $email . "';";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $first_name, $last_name, $email, $password)
	{
		do {
			$this->load->model('subscriber');
			$record = $this->subscriber->getUserByEmail($email);
			
			if ($id > 0) {
				
				$sql =
				"UPDATE `subscriber` SET
    				modified_by = " . $_SESSION['user_id'] . ",
    				first_name = '" . trim(str_replace("'", "\'", $first_name)) . "',
    				last_name = '" . trim(str_replace("'", "\'", $last_name)) . "',
    				email = '" . trim(str_replace("'", "\'", $email)) . "',
					subscribe = 1
    			WHERE
    				id = " . trim(str_replace("'", "\'", $id)) . ";";
			} else {
				if (count($record) > 0) {
					$status = 'Can not use this email. Choose another.';
					break;
				}
				
				$all_codes = $this->user->getCodes();
				
				do {
					$code = '';
					
					$seed = str_split('ABCDEFGHJKLMNPQRSTUVWXYZ23456789');
					
					shuffle($seed); // probably optional since array_is randomized; this may be redundant
					$rand = '';
					foreach (array_rand($seed, 4) as $k) $rand .= $seed[$k];
				} while (isset($all_codes[$rand]));
				
				$sql =
				"INSERT INTO `subscriber` (
    				`created_by`,
    				`first_name`,
    				`last_name`,
    				`email`,
    				`password`
    			) VALUES (
    				'1',
    				'" . trim(str_replace("'", "\'", $first_name)) . "',
    				'" . trim(str_replace("'", "\'", $last_name)) . "',
    				'" . trim(str_replace("'", "\'", $email)) . "',
    				'" . trim(str_replace("'", "\'", $password)) . "');";
				
				
			}
			
			return $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		} while (false);
		
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `vendor` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		if ($status == 1) {
			$sql = 'UPDATE `inventory` SET deleted = 1 WHERE vendor_id = ' . $id;
			
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
	
		return $status;
	}
}