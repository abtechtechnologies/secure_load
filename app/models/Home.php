<?php
class Home extends CI_Model {

        public function __construct()
        {
        	$this->load->database();
        	
        	$this->load->model('client');
        	$client = $this->client->getRecord($_SESSION['client_id']);
        	
        	$this->DB = $this->load->database($client['db_name'], TRUE);
        }

		public function getUsers($reverse = false)
		{
			if ($reverse == true) {
				$sql = 'SELECT * FROM user WHERE deleted = 0 ORDER BY id DESC;';
			} else {
				$sql = 'SELECT * FROM user WHERE deleted = 0;';
			}
			
			$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
			$return = array();
		
			foreach ($db->result_array() as $row) {
				$return[$row['id']] = $row;
			}
		
			return $return;
		}
		
		public function getUser($id)
		{
			$sql = 'SELECT * FROM user WHERE deleted = 0 AND id = ' . $id;
		
			$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
			$return = array();
		
			foreach ($db->result_array() as $row) {
				$return = $row;
			}
		
			return $return;
		}
		
		public function writeData($id, $first_name, $last_name, $email, $phone, $login)
		{
			if ($id > 0) {
				$sql =
					"UPDATE `user` SET
						modified_by = " . $_SESSION['user_id'] . ",
						first_name = '" . str_replace("'", "\'", $first_name) . "',
						last_name = '" . str_replace("'", "\'", $last_name) . "',
						email = '" . str_replace("'", "\'", $email) . "',
						phone = '" . str_replace("'", "\'", $phone) . "',
						login = '" . str_replace("'", "\'", $login) . "'
					WHERE
						id = " . str_replace("'", "\'", $id) . ";";
			} else {
				$sql =
					"INSERT INTO `user` (
						`created_by`,
						`first_name`,
						`last_name`,
						`email`,
						`phone`,
						`login`,
						`user_type_id`
					) VALUES (
						'1',
						'" . str_replace("'", "\'", $first_name) . "',
						'" . str_replace("'", "\'", $last_name) . "',
						'" . str_replace("'", "\'", $email) . "',
						'" . str_replace("'", "\'", $phone) . "',
						'" . str_replace("'", "\'", $login) . "',
						'1');";
			}
			
			return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
		
		public function writePassword($password)
		{
			//Get users in reverse order to grab most recently modified user
			$this->load->model('home');
			$page['users'] = $this->home->getUsers(true);
			
			foreach ($page['users'] as $id => $data) {
				$user_id = $id;
				break;
			}
			
			// A higher "cost" is more secure but consumes more processing power
			$cost = 10;
			
			// Create a random salt
			$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
			
			// Prefix information about the hash so PHP knows how to verify it later.
			// "$2a$" Means we're using the Blowfish algorithm. The following two digits are the cost parameter.
			$salt = sprintf("$2a$%02d$", $cost) . $salt;
			
			// Hash the password with the salt
			$hash = crypt($password, $salt);

			$sql =
				"UPDATE `user` SET
					password = '" . str_replace("'", "\'", $hash) . "'
				WHERE
					id = " . str_replace("'", "\'", $user_id) . ";";
			
			return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
}