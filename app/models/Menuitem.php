<?php
class Menuitem extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecent()
	{
		$sql = 'SELECT * FROM menu_item WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM menu_item WHERE id = ' . $id . ' AND deleted = 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM menu_item WHERE deleted = 0 ORDER BY id;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAlt()
	{
	    $sql = 'SELECT * FROM menu_item WHERE deleted = 0 ORDER BY id;';
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {

	        $return[$row['product_id']][$row['id']] = $row;
	    }
	
	    return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM menu_item WHERE user_id = ' . $id . ' AND deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByProductId($product_id)
	{
	    $sql = 'SELECT * FROM menu_item WHERE product_id = ' . $product_id . ' AND deleted = 0 ORDER BY id;';
	
	    $db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	    $return = array();
	
	    foreach ($db->result_array() as $row) {
	        $return[$row['id']] = $row;
	    }
	
	    return $return;
	}

	public function writeData($menu_item_id, $menu_item_type_id, $name, $price, $attachment, $description, $active)
	{
		if ($menu_item_id == 0) {
			$sql =
			"INSERT INTO `menu_item`
			(
				`created_by`,
				`menu_item_type_id`,
				`name`,
				`price`,
				`description`,
				`attachment_file_name`,
				`active`
			) VALUES (
				'" . $_SESSION['admin_user_id'] . "',
				'" . $menu_item_type_id . "',
				'" . $name . "',
				'" . $price . "',
				'" . $description . "',
				'" . $attachment . "',
				1
			);";
		} else {
			$sql =
			"UPDATE `menu_item` SET
				modified_by = " . $_SESSION['admin_user_id'] . ",
				name = '" . str_replace("'", "\'", trim($name)) . "',
				menu_item_type_id = '" . str_replace("'", "\'", trim($menu_item_type_id)) . "',
				price = '" . str_replace("'", "\'", trim($price)) . "',
				attachment_file_name = '" . str_replace("'", "\'", trim($attachment)) . "',
				description = '" . str_replace("'", "\'", trim($description)) . "',
				active = '" . str_replace("'", "\'", trim($active)) . "'
			WHERE
				id = " . str_replace("'", "\'", $menu_item_id) . ";";
		}
		
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `menu_item` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

}