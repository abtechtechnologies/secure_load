<?php
class Onfleet extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function placeOrder($address, $city, $state, $first_name, $last_name, $phone, $notes, $cart, $cash_due, $order_string) {

		//Strip extra chars and format phone number
		$phone = str_replace(')', '', str_replace('(', '', str_replace('.', '', str_replace('-', '', $phone))));
		$phone = substr_replace($phone, '-', 3, 0);
		$phone = substr_replace($phone, '-', 7, 0);
		
		$key = '64d570652b59cbf9c7a89fce5f0b0b1a';
		
		$data_string = '
		{
			"destination":
			{
				"address":
				{
					"unparsed":"' . $address . ', ' . $city . ', ' . $state . ', USA"
				},
					"notes":"this is a test"
			},
			"recipients":
			[{
				"name":"' . $first_name . ' ' . $last_name . '",
				"phone":"' . $phone . '",
				"notes":"**CLICK HERE TO CALL OR TEXT ' . $first_name . '**"
			}],
			"notes":"Cash Due: $' . $cash_due . '\n******\nOrder: ' . $order_string . '\n******\nOrder Notes: ' . $notes . '",
			"autoAssign":
			{
				"mode":"distance"
			}
		}';
		
		// create curl resource
		$ch = curl_init();
		
		// set url
		curl_setopt($ch, CURLOPT_URL, "https://onfleet.com/api/v2/tasks");
		
		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: Basic ' . base64_encode($key . ':' . ''),
			'Access-Control-Allow-Origin: http://wwweed.org',
			'Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS',
			'Access-Control-Max-Age: 1000',
			'Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With'
		));
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		
		// $output contains the output string
		$output = curl_exec($ch);
		
		// close curl resource to free up system resources
		curl_close($ch);
		
		$status = json_decode($output, JSON_PRETTY_PRINT);
		
		if (isset($status['message'])) {
			if (isset($status['message']['cause'])) {
				$error = $status['message']['cause'];
			} else {
				if (isset($status['message']['message'])) {
					$error = $status['message']['message'];
				} else {
					$error = 'There has been an unknown error routing your delivery, but your order has gone through. We will contact you shortly with any additional information we need.';
				}
			}
			
		} else {
			$error = 0;
		}

		if ($error == 0) {
			return 1;
		} else {
			return $error;
		}
		
// 		$return = array(
// 			'status' => $status,
// 			'error' => $error
// 		);
		
	}
}