<?php
class Contenttype extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecords()
	{
		
		$db = $this->DB->query('SELECT * FROM content_type ORDER BY ID');
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getContentTypeName($content_type_id)
	{
		$db = $this->DB->query('SELECT * FROM content_type WHERE id = ' . $content_type_id);
	
		$return = '';
		
		foreach ($db->result_array() as $row) {
			$return = $row['name'];
		}
	
		return $return;
	}
}