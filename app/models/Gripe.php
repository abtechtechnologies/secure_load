<?php
class Gripe extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM gripe WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords($include_blank = false)
	{				
		$sql = 'SELECT * FROM gripe WHERE deleted = 0 ORDER BY id;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		if ($include_blank == true) {
			$return[0] = array();
			$return[0]['name'] = 'None';
			
		}
				
		foreach ($db->result_array() as $row) {
			
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM gripe WHERE 1;';
	
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecentgripeId()
	{
		$sql = 'SELECT * FROM gripe WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getgripesBygripeType($gripe_type_id)
	{
		$sql = 'SELECT * FROM gripe WHERE deleted = 0 AND parent_id = 0 AND gripe_type_id = ' . $gripe_type_id . ';';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getgripesByParentId($gripe_id)
	{
		$sql = 'SELECT * FROM gripe WHERE deleted = 0 AND parent_id = ' . $gripe_id . ';';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getgripesByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[] = $row['gripe_id'];
		}
	
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM gripe WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getgripeSizes()
	{
		$sql = 'SELECT * FROM gripe WHERE deleted = 0 AND parent_id > 0;';
	
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getUserByEmail($email)
	{
		$sql = "SELECT * FROM subscriber WHERE deleted = 0 AND email = '" . $email . "';";
		
		$db = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $first_name, $last_name, $email, $password)
	{
		do {
			$this->load->model('subscriber');
			$record = $this->subscriber->getUserByEmail($email);
			
			if ($id > 0) {
				
				$sql =
				"UPDATE `subscriber` SET
    				modified_by = " . $_SESSION['user_id'] . ",
    				first_name = '" . trim(str_replace("'", "\'", $first_name)) . "',
    				last_name = '" . trim(str_replace("'", "\'", $last_name)) . "',
    				email = '" . trim(str_replace("'", "\'", $email)) . "',
					subscribe = 1
    			WHERE
    				id = " . trim(str_replace("'", "\'", $id)) . ";";
			} else {
				if (count($record) > 0) {
					$status = 'Can not use this email. Choose another.';
					break;
				}
				
				$all_codes = $this->user->getCodes();
				
				do {
					$code = '';
					
					$seed = str_split('ABCDEFGHJKLMNPQRSTUVWXYZ23456789');
					
					shuffle($seed); // probably optional since array_is randomized; this may be redundant
					$rand = '';
					foreach (array_rand($seed, 4) as $k) $rand .= $seed[$k];
				} while (isset($all_codes[$rand]));
				
				$sql =
				"INSERT INTO `subscriber` (
    				`created_by`,
    				`first_name`,
    				`last_name`,
    				`email`,
    				`password`
    			) VALUES (
    				'1',
    				'" . trim(str_replace("'", "\'", $first_name)) . "',
    				'" . trim(str_replace("'", "\'", $last_name)) . "',
    				'" . trim(str_replace("'", "\'", $email)) . "',
    				'" . trim(str_replace("'", "\'", $password)) . "');";
				
				
			}
			
			return $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		} while (false);
		
		return $status;
	}
	
	public function updateSingleTension($id, $tension, $load)
	{
		$sql = 'UPDATE `gripe` SET current_tension = ' . $tension . ', current_load = "' . $load . '" WHERE id = ' . $id;
		
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
	
	public function setLoad($id, $load)
	{
		$sql = 'UPDATE `gripe` SET current_load = "' . $load . '" WHERE id = ' . $id;

		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `gripe` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		
		if ($status == 1) {
			$sql = 'UPDATE `inventory` SET deleted = 1 WHERE gripe_id = ' . $id;
			
			$status = $this->db->query(preg_replace( '/\s+/', ' ', $sql));
		}
	
		return $status;
	}
}