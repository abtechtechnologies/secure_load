<?php
class Transactioncount extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord()
	{
		$sql = 'SELECT * FROM transaction_count WHERE id = 1 AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$retunr = 0;
		
		foreach ($db->result_array() as $row) {
			$return = $row['count'];
		}
		
		return $return;
	}
	
	public function addTic($count)
	{
		$sql = 'UPDATE `transaction_count` SET count = ' . $count . ' WHERE id = 1';
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM transaction_count WHERE deleted = 0;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM transaction_count WHERE user_id = ' . $id . ' AND deleted = 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function writeData($data)
	{
		if ($data['transaction_count_id'] > 0) {
			$sql =
			"UPDATE `transaction_count` SET
				modified_by = " . $_SESSION['user_id'] . ",
				user_id = '" . str_replace("'", "\'", trim($data['user_id'])) . "',
				product_id = '" . str_replace("'", "\'", $data['product_id']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['transaction_count_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `transaction_count`
			(
				`created_by`, 
				`user_id`, 
				`product_id`
			) VALUES ('
				" . $_SESSION['user_id'] . "', '
				" . str_replace("'", "\'", trim($data['user_id'])) . "', '
				" . str_replace("'", "\'", trim($data['product_id'])) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `content` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}