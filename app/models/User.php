<?php
class User extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();

		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecent()
	{
		$sql = 'SELECT * FROM user WHERE deleted = 0 ORDER BY id DESC LIMIT 1';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecentId()
	{
		$sql = 'SELECT * FROM user WHERE deleted = 0 ORDER BY id DESC LIMIT 1';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getRecords($reverse = false)
	{
		if ($reverse == true) {
			$sql = 'SELECT * FROM user WHERE deleted = 0 ORDER BY id DESC;';
		} else {
			$sql = 'SELECT * FROM user WHERE deleted = 0;';
		}
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getInactive()
	{
		$sql = 'SELECT * FROM user WHERE deleted = 0 AND active = 0 AND user_type_id = 2;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM user WHERE deleted = 0 AND id = ' . $id;
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecordAdmin($id)
	{
		$sql = 'SELECT * FROM user WHERE id = ' . $id;
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin($reverse = false)
	{
		if ($reverse == true) {
			$sql = 'SELECT * FROM user WHERE 1 ORDER BY id DESC;';
		} else {
			$sql = 'SELECT * FROM user WHERE 1;';
		}
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getCodes()
	{
		$sql = 'SELECT * FROM user WHERE 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['referral_code']] = $row['referral_code'];
		}
		
		return $return;
	}
	
	public function getUsers($reverse = false)
	{
		if ($reverse == true) {
			$sql = 'SELECT * FROM user WHERE deleted = 0 ORDER BY id DESC;';
		} else {
			$sql = 'SELECT * FROM user WHERE deleted = 0;';
		}
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getUserByEmail($email)
	{
		$sql = 'SELECT * FROM user WHERE deleted = 0 AND email = ' . '\'' . $email . '\'';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getUserByLogin($login)
	{
		$sql = 'SELECT * FROM user WHERE deleted = 0 AND login = ' . '\'' . $login . '\'';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getDrivers($list = false)
	{
		$sql = 'SELECT * FROM user WHERE user_type_id IN (1,3) AND deleted = 0 ORDER BY id DESC;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		if ($list == true) {
			$this->load->model('companyinfo');
			$company = $this->companyinfo->getRecord();
			
			$ids = explode(',', $company['excluded_user_ids']);
			
			foreach ($ids as $i => $user_id) {
				if (isset($return[$user_id])) {
					unset($return[$user_id]);
				}
			}
		}
		
		return $return;
	}
	
	public function getEmployees($list = false)
	{
		$sql = 'SELECT * FROM user WHERE user_type_id = 5 AND deleted = 0 ORDER BY id DESC;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		if ($list == true) {
			$this->load->model('companyinfo');
			$company = $this->companyinfo->getRecord();
			
			$ids = explode(',', $company['excluded_user_ids']);
			
			foreach ($ids as $i => $user_id) {
				if (isset($return[$user_id])) {
					unset($return[$user_id]);
				}
			}
		}
		
		return $return;
	}
	
	public function getReps($list = false)
	{
		$sql = 'SELECT * FROM user WHERE user_type_id = 6 AND deleted = 0 ORDER BY id DESC;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		if ($list == true) {
			$this->load->model('companyinfo');
			$company = $this->companyinfo->getRecord();
			
			$ids = explode(',', $company['excluded_user_ids']);
			
			foreach ($ids as $i => $user_id) {
				if (isset($return[$user_id])) {
					unset($return[$user_id]);
				}
			}
		}
		
		return $return;
	}
	
	public function getAvailableHolders($list = false, $start_blank = false, $choose = false)
	{
		$sql = 'SELECT * FROM user WHERE user_type_id IN (5, 3, 1, 6) AND deleted = 0 ORDER BY id DESC;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('user');
		$users = $this->user->getRecords();
		
		if ($start_blank == true) {
			$return[0] = array();
			$return[0]['first_name'] = '- Select';
			$return[0]['last_name'] = ' User -';
		}
		
		if ($choose == true) {
			$return[0] = array();
			$return[0]['first_name'] = '- DEFAULT';
			$return[0]['last_name'] = '  (' . trim(ucfirst($users[$company['primary_inventory_user_id']]['first_name'])) . ' ' . trim(ucfirst($users[$company['primary_inventory_user_id']]['last_name'])) . ') -';
		}
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		if ($choose == true) {
			if (isset($return[$company['primary_inventory_user_id']])) {
				unset($return[$company['primary_inventory_user_id']]);
			}
		}
		
		if ($list == true) {
			
			$ids = explode(',', $company['excluded_user_ids']);

			foreach ($ids as $i => $user_id) {
				if (isset($return[$user_id])) {
					unset($return[$user_id]);
				}
			}
		}
		
		unset($return[1]);
		
		return $return;
	}
	
	public function getPhoneOperators()
	{
		$sql = 'SELECT * FROM user WHERE user_type_id = 4 AND deleted = 0 ORDER BY id DESC;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getClients($last_name = false)
	{
		if ($last_name == true) {
			$sql = 'SELECT * FROM user WHERE user_type_id = 2 OR id = 2 AND deleted = 0 ORDER BY last_name;';
		} else {
			$sql = 'SELECT * FROM user WHERE user_type_id = 2 OR id = 2 AND deleted = 0 ORDER BY first_name;';
		}
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getCustomers()
	{
		$sql = 'SELECT * FROM user WHERE user_type_id = 2 AND deleted = 0 ORDER BY last_name;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getAdmins($list = false)
	{
		$sql = 'SELECT * FROM user WHERE user_type_id = 1 AND deleted = 0 ORDER BY id DESC;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		if ($list == true) {
			$this->load->model('companyinfo');
			$company = $this->companyinfo->getRecord();
			
			$ids = explode(',', $company['excluded_user_ids']);
			
			foreach ($ids as $i => $user_id) {
				if (isset($return[$user_id])) {
					unset($return[$user_id]);
				}
			}
		}
		
		return $return;
	}
	
	public function getEmails()
	{
		$sql = 'SELECT * FROM user WHERE deleted = 0';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row['email'];
		}
		
		return $return;
	}
	
	public function getReferrals($user_id)
	{
		$sql = 'SELECT * FROM user WHERE deleted = 0 AND referral_id = ' . $user_id . ' ORDER BY id DESC;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getUser($id)
	{
		$sql = 'SELECT * FROM user WHERE deleted = 0 AND id = ' . $id;
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function writeData($id, $first_name, $last_name, $email, $user_type_id, $phone = '', $login = '', $address = '', $city = '', $zip = '', $membership = 0, $balance = 0, $referral = '', $discover = '', $id_file_name = '', $rec_file_name = '', $referral_code_used = '')
	{
		do {
			$this->load->model('user');
			$record = $this->user->getUserByEmail($email);
			$record2 = $this->user->getUserByLogin($login);
			
			if ($id > 0) {
				if (count($record) > 0) {
					if ($record['id'] != $id) {
						$status = 'Can not use this email. Choose another.';
						break;
					}
				}
				
				if ($login != '') {
					if (count($record2) > 0) {
						if ($record['id'] != $id) {
							$status = 'Can not use this login name. Choose another.';
							break;
						}
					}
				}
				
				$sql =
				"UPDATE `user` SET
    				modified_by = " . $_SESSION['user_id'] . ",
    				first_name = '" . trim(str_replace("'", "\'", $first_name)) . "',
    				last_name = '" . trim(str_replace("'", "\'", $last_name)) . "',
    				email = '" . trim(str_replace("'", "\'", $email)) . "',
    				phone = '" . trim(str_replace("'", "\'", $phone)) . "',
    				login = '" . trim(str_replace("'", "\'", $login)) . "',
    				city = '" . trim(str_replace("'", "\'", $city)) . "',
    				zip = '" . trim(str_replace("'", "\'", $zip)) . "',
    				membership = '" . trim(str_replace("'", "\'", $membership)) . "',
    				balance = '" . trim(str_replace("'", "\'", $balance)) . "',
    				referral_name = '" . trim(str_replace("'", "\'", $referral)) . "',
    				discover = '" . trim(str_replace("'", "\'", $discover)) . "',
    				id_file_name = '" . trim(str_replace("'", "\'", $id_file_name)) . "',
    				rec_file_name = '" . trim(str_replace("'", "\'", $rec_file_name)) . "',
    				address = '" . trim(str_replace("'", "\'", $address)) . "'
    			WHERE
    				id = " . trim(str_replace("'", "\'", $id)) . ";";
			} else {
				if (count($record) > 0) {
					$status = 'Can not use this email. Choose another.';
					break;
				}
				
				if ($login != '') {
					if (count($record2) > 0) {
						$status = 'Can not use this login. Choose another.';
						break;
					}
				}
				
				$all_codes = $this->user->getCodes();
				
				do {
					$code = '';
					
					$seed = str_split('ABCDEFGHJKLMNPQRSTUVWXYZ23456789');
					
					shuffle($seed); // probably optional since array_is randomized; this may be redundant
					$rand = '';
					foreach (array_rand($seed, 4) as $k) $rand .= $seed[$k];
				} while (isset($all_codes[$rand]));
				
				$sql =
				"INSERT INTO `user` (
    				`created_by`,
    				`first_name`,
    				`last_name`,
    				`email`,
    				`phone`,
    				`login`,
    				`user_type_id`,
    				`city`,
    				`zip`,
    				`membership`,
    				`balance`,
    				`referral_name`,
					`referral_code`,
					`referral_code_used`,
    				`discover`,
    				`id_file_name`,
    				`rec_file_name`,
    				`address`
    			) VALUES (
    				'1',
    				'" . trim(str_replace("'", "\'", $first_name)) . "',
    				'" . trim(str_replace("'", "\'", $last_name)) . "',
    				'" . trim(str_replace("'", "\'", $email)) . "',
    				'" . trim(str_replace("'", "\'", $phone)) . "',
    				'" . trim(str_replace("'", "\'", $login)) . "',
    				'" . trim(str_replace("'", "\'", $user_type_id)) . "',
    				'" . trim(str_replace("'", "\'", $city)) . "',
    				'" . trim(str_replace("'", "\'", $zip)) . "',
    				'" . trim(str_replace("'", "\'", $membership)) . "',
    				'" . trim(str_replace("'", "\'", $balance)) . "',
    				'" . trim(str_replace("'", "\'", $referral)) . "',
					'" . $rand . "',
					'" . trim(str_replace("'", "\'", $referral_code_used)) . "',
    				'" . trim(str_replace("'", "\'", $discover)) . "',
    				'" . trim(str_replace("'", "\'", $id_file_name)) . "',
    				'" . trim(str_replace("'", "\'", $rec_file_name)) . "',
    				'" . trim(str_replace("'", "\'", $address)) . "');";
				
				
			}
			
			return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		} while (false);
		
		return $status;
	}
	
	public function activate($id)
	{
		$sql =
		"UPDATE `user` SET
				modified_by = " . $_SESSION['user_id'] . ",
				active = 1
			WHERE
				id = " . trim(str_replace("'", "\'", $id)) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function updateBalance($user_id, $subtotal)
	{
		$sql = 'SELECT balance FROM user WHERE deleted = 0 AND id = ' . $user_id;
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row['balance'];
		}
		
		$balance = $return - $subtotal;
		
		if ($balance < 0) {
			$balance = 0;
		}
		
		$sql =
		"UPDATE `user` SET
			balance = " . $balance . "
		WHERE
			id = " . trim(str_replace("'", "\'", $user_id)) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function updateBalanceAdmin($user_id, $balance)
	{
		$sql =
		"UPDATE `user` SET
			balance = " . $balance . "
		WHERE
			id = " . trim(str_replace("'", "\'", $user_id)) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function updateMembershipPaid($user_id, $i)
	{
		$sql =
		"UPDATE `user` SET
			membership_paid = " . $i . "
		WHERE
			id = " . trim(str_replace("'", "\'", $user_id)) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function writePassword($password, $user_id = 0)
	{
		if ($user_id == 0) {
			//Get users in reverse order to grab most recently modified user
			$this->load->model('user');
			$page['users'] = $this->user->getUsers(true);
			
			foreach ($page['users'] as $id => $data) {
				$user_id = $id;
				break;
			}
		}
		
		// A higher "cost" is more secure but consumes more processing power
		$cost = 10;
		
		// Create a random salt
		$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
		
		// Prefix information about the hash so PHP knows how to verify it later.
		// "$2a$" Means we're using the Blowfish algorithm. The following two digits are the cost parameter.
		$salt = sprintf("$2a$%02d$", $cost) . $salt;
		
		// Hash the password with the salt
		$hash = crypt($password, $salt);
		
		$sql =
		"UPDATE `user` SET
				password = '" . str_replace("'", "\'", $hash) . "'
			WHERE
				id = " . str_replace("'", "\'", $user_id) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function payMembership($user_id, $membership)
	{
		$this->load->model('membership');
		$membership = $this->membership->getRecord($membership);
		
		$this->load->model('user');
		$user = $this->user->getRecord($user_id);
		
		$balance = $user['balance'] + $membership['worth'];
		
		$sql =
		"UPDATE `user` SET
			membership_paid = 1,
			balance = " . $balance . "
		WHERE
			id = " . $user_id . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function writeCode($user_id, $code)
	{
		
		$sql =
		"UPDATE `user` SET
				referral_code = '" . str_replace("'", "\'", $code) . "'
			WHERE
				id = " . str_replace("'", "\'", $user_id) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function setTheme($user_id, $theme)
	{
		
		$sql =
		"UPDATE `user` SET
				theme = '" . str_replace("'", "\'", $theme) . "'
			WHERE
				id = " . str_replace("'", "\'", $user_id) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function moveBalance($to, $from, $amount)
	{
		$this->load->model('user');
		$user1 = $this->user->getRecord($to);
		$user2 = $this->user->getRecord($from);
		
		$new_balance1 = $user1['balance'] - $amount;
		$new_balance2 = $user2['balance'] + $amount;
		
		$sql =
		"UPDATE `user` SET
			balance = '" . $new_balance1 . "'
		WHERE
			id = " . $to . ";";
		
		$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		if ($status == 1) {
			$sql =
			"UPDATE `user` SET
    			balance = '" . $new_balance2 . "'
    		WHERE
    			id = " . $from . ";";
			
			$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
		
		print $status;
	}
	
	public function search($query, $member)
	{
		$sql = "
			SELECT * FROM user WHERE deleted = 0
			AND first_name LIKE '%" . $query . "%'
			OR last_name LIKE '%" . $query . "%'
			OR phone LIKE '%" . $query . "%'
			ORDER BY id DESC;";
		
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if ($member == true) {
				if ($row['membership'] > 0) {
					$return[$row['id']] = $row;
				}
			} else {
				$return[$row['id']] = $row;
			}
		}
		
		return $return;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `user` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
}