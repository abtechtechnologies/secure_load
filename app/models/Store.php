<?php
class Store extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function purchaseMembership($user_id, $membership_id)
	{
		$this->load->model('companyinfo');
		$page_data['company_info'] = $this->companyinfo->getRecord();
		
		$this->load->model('sale');
		$this->load->model('user');
		$this->load->model('cart');
		$this->load->model('transactioncount');
		$transaction = $this->transactioncount->getRecord();
		$transaction += 1;
		
		$status = 1;
		
		switch ($membership_id) {
			case 1:
				$product_id = 93;
				$due = 150;
				break;
			case 2:
				$product_id = 95;
				$due = 300;
				break;
			default:
				$product_id = 97;
				$due = 500;
		}

		$sale_array = array(
			'sale_id' => 0,
			'transaction' => $transaction,
			'user_id' => $user_id,
			'product_id' => $product_id
		);
			
		$status = $this->sale->writeData($sale_array);
	
		
		//Record Transaction
		if ($status == 1) {
			$this->load->model('transactiontotal');
			$status = $this->transactiontotal->writeData($transaction, $due);
		}

		//Increase transaction count
		if ($status == 1) {
			$status = $this->transactioncount->addTic($transaction);
		}
		
		return $status;
	}
	
	public function getRecords($include_children = false)
	{			
		if ($include_children == true) {
			$sql = 'SELECT * FROM product_type WHERE deleted = 0 ORDER BY id;';
		} else {
			$sql = 'SELECT * FROM product_type WHERE deleted = 0 AND parent_id = 0 ORDER BY id;';
		}

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}

	public function writeData($data)
	{
		if ($data['product_type_id'] > 0) {
			$sql =
			"UPDATE `product_type` SET
				modified_by = " . $_SESSION['user_id'] . ",
				name = '" . str_replace("'", "\'", $data['name']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['product_type_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `product_type`
			(
				`created_by`,  
				`name`
			) VALUES ('
				" . $_SESSION['user_id'] . "', '
				" . str_replace("'", "\'", $data['name']) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `product_type` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}