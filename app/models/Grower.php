<?php
class Grower extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM grower WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords($include_blank = false)
	{				
		$sql = 'SELECT * FROM grower WHERE deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		if ($include_blank == true) {
			$return[0] = array();
			$return[0]['name'] = 'None';
			
		}
				
		foreach ($db->result_array() as $row) {
			
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM grower WHERE 1;';
	
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecentgrowerId()
	{
		$sql = 'SELECT * FROM grower WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getgrowersBygrowerType($grower_type_id)
	{
		$sql = 'SELECT * FROM grower WHERE deleted = 0 AND parent_id = 0 AND grower_type_id = ' . $grower_type_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getgrowersByParentId($grower_id)
	{
		$sql = 'SELECT * FROM grower WHERE deleted = 0 AND parent_id = ' . $grower_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getgrowersByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[] = $row['grower_id'];
		}
	
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM grower WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getgrowerSizes()
	{
		$sql = 'SELECT * FROM grower WHERE deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function writeData($data)
	{
		if ($data['grower_id'] > 0) {
			$sql =
			"UPDATE `grower` SET
				modified_by = " . $_SESSION['user_id'] . ",
				rep_name = '" . str_replace("'", "\'", trim($data['rep_name'])) . "',
				phone = '" . str_replace("'", "\'", trim($data['phone'])) . "',
				address = '" . str_replace("'", "\'", trim($data['address'])) . "',
				website = '" . str_replace("'", "\'", trim($data['website'])) . "',
				name = '" . str_replace("'", "\'", trim($data['name'])) . "',
				description = '" . str_replace("'", "\'", trim($data['description'])) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['grower_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `grower`
			(
				`created_by`, 
				`rep_name`, 
				`phone`, 
				`address`, 
				`website`, 
				`name`, 
				`description`, 
				`attachment_file_name`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", trim($data['rep_name'])) . "', 
				'" . str_replace("'", "\'", trim($data['phone'])) . "', 
				'" . str_replace("'", "\'", trim($data['address'])) . "',
				'" . str_replace("'", "\'", trim($data['website'])) . "', 
				'" . str_replace("'", "\'", trim($data['name'])) . "', 
				'" . str_replace("'", "\'", trim($data['description'])) . "', 
				'" . str_replace("'", "\'", trim($data['attachment'])) . "'
			);";
		}
		
		$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `grower` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		if ($status == 1) {
			$sql = 'UPDATE `inventory` SET deleted = 1 WHERE grower_id = ' . $id;
			
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
	
		return $status;
	}
}