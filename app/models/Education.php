<?php
class Education extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM education WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM education WHERE deleted = 0;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($data)
	{
		$status = 1; 
		
		if ($data['education_id'] > 0) {
			$sql =
				"UPDATE `education` SET
					modified_by = " . $_SESSION['user_id'] . ",
					title = '" . $data['title'] . "',
					content_text = '" . str_replace("'", "\'", $data['content_text']) . "',
					attachment_file_name = '" . $data['attachment'] . "'
				WHERE
					id = " . $data['education_id'] . ";";
		} else {
			$sql =
				"INSERT INTO `education`
			(
				`created_by`,
				`title`,
				`content_text`,
				`attachment_file_name`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", trim($data['title'])) . "',
				'" . str_replace("'", "\'", $data['content_text']) . "',
				'" . $data['attachment'] . "'
			);";
		}
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}

	public function updateDB($education_id)
	{
		$status = 1;
	
		for ($count = 1; $count < 6; $count++) {
			if ($status == 1) {
				$sql =
				"INSERT INTO `education`
				(
					`created_by`,
					`education_id`,
					`count`,
					`attachment_file_name`
				) VALUES (
					'" . $_SESSION['user_id'] . "',
					'" . str_replace("'", "\'", trim($education_id)) . "',
					'" . $count . "',
					''
				);";
			}
				
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
	
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `education` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}