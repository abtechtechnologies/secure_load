<?php
class Inventory extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();

		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM inventory WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getUserRecord($user_id, $product_id)
	{
		$sql = 'SELECT * FROM inventory WHERE user_id = ' . $user_id . ' AND product_id = ' . $product_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getMatch($user_id, $product_id)
	{
		$sql = 'SELECT * FROM inventory WHERE user_id = ' . $user_id . ' AND product_id = ' . $product_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function findMatch($user_id, $product_id)
	{
		$sql = 'SELECT * FROM inventory WHERE user_id = ' . $user_id . ' AND product_id = ' . $product_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getUserRecords($user_id)
	{
		$sql = 'SELECT * FROM inventory WHERE user_id = ' . $user_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductsWithoutHolder($user_id)
	{
		$sql = 'SELECT * FROM inventory WHERE user_id = ' . $user_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$all_products = array();
		$products_pass = array();
		$duplicates = array();
		
		foreach ($db->result_array() as $row) {
			if (isset($products_pass[$row['product_id']])) {
				$duplicates[$row['product_id']] = $row['product_id'];
			} else {
				$products_pass[$row['product_id']] = $row['product_id'];
			}
		}
		
		$sql = 'SELECT * FROM inventory WHERE deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$all_products[$row['product_id']] = $row['product_id'];
		}
		
		foreach ($all_products as $product_id) {
			if (isset($products_pass[$product_id])) {
				unset($all_products[$product_id]);
			}
		}
		
		return $all_products;
	}
	
	public function getHouseProduct($product_id)
	{
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('product');
		$product = $this->product->getRecord($product_id);
		
		if ($product['inventory_holder_id'] > 0) {
			$user_id = $product['inventory_holder_id'];
		} else {
			$user_id = $company['primary_inventory_user_id'];
		}
		
		$sql = 'SELECT * FROM inventory WHERE product_id = ' . $product_id . ' AND user_id = ' . $user_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords()
	{
		$sql = 'SELECT * FROM inventory WHERE deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getDuplicates()
	{
		$sql = 'SELECT * FROM inventory WHERE deleted = 0 AND user_id = 4;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		$match = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['product_id']])) {
				$return[$row['product_id']] = 1;
			} else {
				$match[$row['product_id']] = 1;
			}
		}
		
		return $match;
	}
	
	public function getProductInventory($product_id)
	{
		$sql = 'SELECT * FROM inventory WHERE product_id = ' . $product_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return += $row['quantity'];
		}
		
		return $return;
	}
	
	public function getHouseInventory($product_id)
	{
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('product');
		$product = $this->product->getRecord($product_id);
		
		if ($product['inventory_holder_id'] > 0) {
			$user_id = $product['inventory_holder_id'];
		} else {
			$user_id = $company['primary_inventory_user_id'];
		}
		
		$sql = 'SELECT * FROM inventory WHERE product_id = ' . $product_id . ' AND user_id = ' . $user_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return += $row['quantity'];
		}
		
		return $return;
	}
	
	public function getHouseRecord($product_id)
	{
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('product');
		$product = $this->product->getRecord($product_id);
		
		if ($product['inventory_holder_id'] > 0) {
			$user_id = $product['inventory_holder_id'];
		} else {
			$user_id = $company['primary_inventory_user_id'];
		}
		
		$sql = 'SELECT * FROM inventory WHERE product_id = ' . $product_id . ' AND user_id = ' . $user_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return =  $row;
		}
		
		return $return;
	}
	
	public function getRecordByProductId($product_id)
	{
		$sql = 'SELECT * FROM inventory WHERE product_id = ' . $product_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByProductId($product_id, $sort_by_user = false)
	{
		$sql = 'SELECT * FROM inventory WHERE product_id = ' . $product_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if ($sort_by_user == true) {
				$return[$row['user_id']] = $row;
			} else {
				$return[$row['id']] = $row;
			}
		}
		
		return $return;
	}
	
	public function getRecordsByProductTypeId($product_type_id)
	{
		$sql = 'SELECT * FROM inventory WHERE product_type_id = ' . $product_type_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByUser()
	{
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('product');
		
		$sql = 'SELECT * FROM inventory WHERE deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['user_id']])) {
				$return[$row['user_id']] = array();
			}

			$return[$row['user_id']][$row['product_id']] = $row;
			
		}
		
		return $return;
	}
	
	public function getRecordsByProduct()
	{
		$sql = 'SELECT * FROM inventory WHERE deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['product_id']])) {
				$return[$row['product_id']] = array();
			}
			
			$return[$row['product_id']][$row['user_id']] = $row;
		}
		
		return $return;
	}
	
	public function getPrimaryHouseRecords()
	{
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		$user_id = $company['primary_inventory_user_id'];
		
		
		$sql = 'SELECT * FROM inventory WHERE deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if ($row['user_id'] == $user_id) {
				$return[$row['product_id']] = $row;
			}
		}
		
		return $return;
	}
	
	public function getHouseRecords()
	{
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('product');
		
		$sql = 'SELECT * FROM inventory WHERE deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$product = $this->product->getRecord($row['product_id']);
			
			if ($product['inventory_holder_id'] > 0) {
				$user_id = $product['inventory_holder_id'];
			} else {
				$user_id = $company['primary_inventory_user_id'];
			}
			
			if ($row['user_id'] == $user_id) {
				
				$return[$row['product_id']] = $row;
			}
		}
		
		return $return;
	}
	
	
	public function writeData($inventory_id, $quantity, $product_id = 0, $user_id = 0, $unit_name = '')
	{
		$_SESSION['user_id'] = 2;
		if ($inventory_id > 0) {
			
			if ($product_id == 0 && $user_id == 0) {
				$sql =
				"UPDATE `inventory` SET
					modified_by = " . $_SESSION['user_id'] . ",
unit_name = '" . $unit_name . "',
					quantity = '" . str_replace("'", "\'", trim($quantity)) . "'
				WHERE
					id = " . str_replace("'", "\'", $inventory_id) . ";";
			} else if ($product_id > 0 && $user_id == 0) {
				$sql =
				"UPDATE `inventory` SET
					modified_by = " . $_SESSION['user_id'] . ",
					quantity = '" . str_replace("'", "\'", trim($quantity)) . "',
unit_name = '" . $unit_name . "',
					product_id = '" . str_replace("'", "\'", $product_id) . "'
				WHERE
					id = " . str_replace("'", "\'", $inventory_id) . ";";
			} else if ($product_id == 0 && $user_id > 0) {
				$sql =
				"UPDATE `inventory` SET
					modified_by = " . $_SESSION['user_id'] . ",
					quantity = '" . str_replace("'", "\'", trim($quantity)) . "',
unit_name = '" . $unit_name . "',
					user_id = '" . str_replace("'", "\'", $user_id) . "'
				WHERE
					id = " . str_replace("'", "\'", $inventory_id) . ";";
			} else {
				$sql =
				"UPDATE `inventory` SET
					modified_by = " . $_SESSION['user_id'] . ",
					quantity = '" . str_replace("'", "\'", trim($quantity)) . "',
					user_id = '" . str_replace("'", "\'", trim($user_id)) . "',
unit_name = '" . $unit_name . "',
					product_id = '" . str_replace("'", "\'", $product_id) . "'
				WHERE
					id = " . str_replace("'", "\'", $inventory_id) . ";";
			}
			
		} else {
			$sql =
			"INSERT INTO `inventory`
			(
				`created_by`,
				`quantity`,
				`product_id`,
`unit_name`,
				`user_id`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", trim($quantity)) . "',
				'" . str_replace("'", "\'", trim($product_id)) . "',
'" . str_replace("'", "\'", trim($unit_name)) . "',
				'" . str_replace("'", "\'", trim($user_id)) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function writeNewHolder($user_id, $quantity, $product_id)
	{
		$sql =
		"INSERT INTO `inventory`
			(
				`created_by`,
				`quantity`,
				`product_id`,
				`user_id`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", trim($quantity)) . "',
				'" . str_replace("'", "\'", trim($product_id)) . "',
				'" . str_replace("'", "\'", trim($user_id)) . "'
			);";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function writeNewHolderFromHouse($user_id, $quantity, $product_id, $new_house, $house_id)
	{
		$sql =
		"INSERT INTO `inventory`
			(
				`created_by`,
				`quantity`,
				`product_id`,
				`user_id`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", trim($quantity)) . "',
				'" . str_replace("'", "\'", trim($product_id)) . "',
				'" . str_replace("'", "\'", trim($user_id)) . "'
			);";
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		if ($status == 1) {
			$sql = 'UPDATE `inventory` SET quantity = ' . $new_house . ' WHERE id = ' . $house_id;
			
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
			
			return $status;
		}
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `inventory` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function removeInventory($product_size_id, $amount_to_reduce, $holder_id = 0)
	{
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$status = 1;
		
		$this->load->model('productsize');
		$product_size = $this->productsize->getRecord($product_size_id);
		
		$product_id = $product_size['product_id'];
		
		$this->load->model('product');
		$product = $this->product->getRecord($product_id);
		
		if ($holder_id > 0) {
			$user_id = $holder_id;
		} else {
			if ($product['inventory_holder_id'] > 0) {
				$user_id = $product['inventory_holder_id'];
			} else {
				$user_id = $company['primary_inventory_user_id'];
			}
		}
		
		
		$quantity = $this->getUserRecord($user_id, $product_id);
		
		if (count($quantity) > 0) {
			$holder_quantity = $quantity['quantity'];
		} else {
			$holder_quantity = 0;
		}
		
		//Holder has enough to cover sale
		if ($holder_quantity >= $amount_to_reduce) {
			$difference = $holder_quantity - $amount_to_reduce;
			
			//Update house record
			$sql = 'UPDATE `inventory` SET quantity = ' . $difference . ' WHERE product_id = ' . $product_id . ' AND user_id = ' . $user_id;

			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
			
		} else {
			$difference = $holder_quantity - $amount_to_reduce;
			
			//Update holder record
			$sql = 'UPDATE `inventory` SET quantity = ' . $difference . ' WHERE product_id = ' . $product_id . ' AND user_id = ' . $user_id;
			
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
			
		}
		
		return $status;
	}
}