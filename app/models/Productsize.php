<?php
class Productsize extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM product_size WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecentId()
	{
		$sql = 'SELECT * FROM product_size WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getUnitPrice($product_id)
	{
		$sql = 'SELECT * FROM product_size WHERE product_id = ' . $product_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		$count = 0;
		
		foreach ($db->result_array() as $row) {		

			if ($row['units'] > 0) {
				$return += $row['price'] / $row['units'];
				$count++;
			}
		}
		
		if ($return > 0) {
			$return = $return / $count;
		}
		
		return $return;
	}
	
	public function getRecordAdmin($id)
	{
		$sql = 'SELECT * FROM product_size WHERE id = ' . $id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords()
	{			
		$sql = 'SELECT * FROM product_size WHERE deleted = 0 ORDER BY id;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM product_size WHERE 1 ORDER BY id;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByProductId($id)
	{
		$sql = 'SELECT * FROM product_size WHERE product_id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductSizesByTransactionId($id)
	{
		$sql = 'SELECT * FROM product_size WHERE product_id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsByProduct()
	{
		$sql = 'SELECT * FROM product_size WHERE deleted = 0 ORDER BY id;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if (!isset($return[$row['product_id']])) {
				$return[$row['product_id']] = array();
			}
			
			$return[$row['product_id']][$row['id']] = $row;
		}
		
		return $return;
	}

	public function writeData($data)
	{

		if ($data['product_size_id'] > 0) {
			$sql =
			"UPDATE `product_size` SET
				modified_by = " . $_SESSION['user_id'] . ",
				name = '" . str_replace("'", "\'", $data['name']) . "',
				units = '" . str_replace("'", "\'", $data['units']) . "',
				price = '" . str_replace("'", "\'", $data['price']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['product_size_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `product_size`
			(
				`created_by`,  
				`name`,
				`units`,
				`price`,
				`product_id`,
				`notes`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", $data['attachment']) . "', 
				'" . str_replace("'", "\'", $data['attachment']) . "', 
				'" . str_replace("'", "\'", $data['price']) . "', 
				'" . str_replace("'", "\'", $data['product_id']) . "', 
				'" . str_replace("'", "\'", $data['notes']) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function write($product_size_id, $product_id, $name, $units, $price)
	{
		
		if ($product_size_id > 0) {
			$sql =
			"UPDATE `product_size` SET
				modified_by = " . $_SESSION['user_id'] . ",
				name = '" . str_replace("'", "\'", $name) . "',
				units = '" . str_replace("'", "\'", $units) . "',
				price = '" . str_replace("'", "\'", $price) . "'
			WHERE
				id = " . str_replace("'", "\'", $product_size_id) . ";";
		} else {
			$sql =
			"INSERT INTO `product_size`
			(
				`created_by`,
				`name`,
				`units`,
				`price`,
				`product_id`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", $name) . "',
				'" . str_replace("'", "\'", $units) . "',
				'" . str_replace("'", "\'", $price) . "',
				'" . str_replace("'", "\'", $product_id) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function writeNewSize($product_id)
	{
		if ($product_id == 0) {
			
		} else {
			$sql =
			"INSERT INTO `product_size`
			(
				`created_by`,
				`product_id`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", $product_id) . "'
			);";
		}
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
		
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `product_size` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}