<?php
class Inventoryarchive extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM inventory_archive WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM inventory_archive WHERE deleted = 0;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductInventory($product_id)
	{
		$sql = 'SELECT * FROM inventory_archive WHERE product_id = ' . $product_id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = 0;
		
		foreach ($db->result_array() as $row) {
			$return += $row['quantity'];
		}
		
		return $return;
	}
	
	public function getRecordByProductId($product_id)
	{
		$sql = 'SELECT * FROM inventory_archive WHERE product_id = ' . $product_id . ' AND deleted = 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}

	
	public function writeData($inventory_archive_id, $quantity, $product_id ,$user_id = 0)
	{
		if ($inventory_archive_id > 0) {
			$sql =
			"UPDATE `inventory_archive` SET
				modified_by = " . $_SESSION['user_id'] . ",
				quantity = '" . str_replace("'", "\'", trim($quantity)) . "',
				user_id = '" . str_replace("'", "\'", trim($user_id)) . "',
				product_id = '" . str_replace("'", "\'", $product_id) . "'
			WHERE
				id = " . str_replace("'", "\'", $inventory_archive_id) . ";";
		} else {
			$sql =
			"INSERT INTO `inventory_archive`
			(
				`created_by`, 
				`quantity`, 
				`product_id`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", trim($quantity)) . "', 
				'" . str_replace("'", "\'", trim($product_id)) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function writeNewHolder($user_id, $quantity, $product_id)
	{
		$sql =
			"INSERT INTO `inventory_archive`
			(
				`created_by`,
				`quantity`,
				`product_id`,
				`user_id`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", trim($quantity)) . "', 
				'" . str_replace("'", "\'", trim($product_id)) . "', 
				'" . str_replace("'", "\'", trim($user_id)) . "'
			);";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `inventory_archive` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
	
	public function removeInventory($product_id, $amount_too_reduce)
	{
		$quantity = $this->getProductInventory($product_id);

		$new_quantity = $quantity - $amount_too_reduce;
		
		$sql = 'UPDATE `inventory_archive` SET quantity = ' . $new_quantity . ' WHERE product_id = ' . $product_id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}