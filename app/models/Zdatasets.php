<?php
class Zdatasets extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM ZDATASETS WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords($include_blank = false)
	{
		$sql = 'SELECT * FROM ZDATASETS WHERE 1 ORDER BY id;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		if ($include_blank == true) {
			$return[0] = array();
			$return[0]['name'] = 'None';
			
		}
		
		foreach ($db->result_array() as $row) {
			
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM ZDATASETS WHERE 1;';
		
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecentZDATASETSId()
	{
		$sql = 'SELECT * FROM ZDATASETS WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
		
		return $return;
	}
	
	public function getZDATASETSsByZDATASETSType($ZDATASETS_type_id)
	{
		$sql = 'SELECT * FROM ZDATASETS WHERE deleted = 0 AND parent_id = 0 AND ZDATASETS_type_id = ' . $ZDATASETS_type_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getZDATASETSsByParentId($ZDATASETS_id)
	{
		$sql = 'SELECT * FROM ZDATASETS WHERE deleted = 0 AND parent_id = ' . $ZDATASETS_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getZDATASETSsByTransactionId($transaction_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND transaction_id = ' . $transaction_id . ';';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[] = $row['ZDATASETS_id'];
		}
		
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM ZDATASETS WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getZDATASETSSizes()
	{
		$sql = 'SELECT * FROM ZDATASETS WHERE deleted = 0 AND parent_id > 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function writeData($data)
	{
		if ($data['ZDATASETS_id'] > 0) {
			$sql =
			"UPDATE `ZDATASETS` SET
				modified_by = " . $_SESSION['user_id'] . ",
				rep_name = '" . str_replace("'", "\'", trim($data['rep_name'])) . "',
				phone = '" . str_replace("'", "\'", trim($data['phone'])) . "',
				address = '" . str_replace("'", "\'", trim($data['address'])) . "',
				website = '" . str_replace("'", "\'", trim($data['website'])) . "',
				name = '" . str_replace("'", "\'", trim($data['name'])) . "',
				description = '" . str_replace("'", "\'", trim($data['description'])) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['ZDATASETS_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `ZDATASETS`
			(
				`created_by`,
				`rep_name`,
				`phone`,
				`address`,
				`website`,
				`name`,
				`description`,
				`attachment_file_name`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", trim($data['rep_name'])) . "',
				'" . str_replace("'", "\'", trim($data['phone'])) . "',
				'" . str_replace("'", "\'", trim($data['address'])) . "',
				'" . str_replace("'", "\'", trim($data['website'])) . "',
				'" . str_replace("'", "\'", trim($data['name'])) . "',
				'" . str_replace("'", "\'", trim($data['description'])) . "',
				'" . str_replace("'", "\'", trim($data['attachment'])) . "'
			);";
		}
		
		$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `ZDATASETS` SET deleted = 1 WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
}