<?php
class Producttype extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM product_type WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords($include_children = false)
	{			
		if ($include_children == true) {
			$sql = 'SELECT * FROM product_type WHERE deleted = 0 ORDER BY id;';
		} else {
			$sql = 'SELECT * FROM product_type WHERE deleted = 0 AND parent_id = 0 ORDER BY id;';
		}

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}

	public function writeData($data)
	{
		if ($data['product_type_id'] > 0) {
			$sql =
			"UPDATE `product_type` SET
				modified_by = " . $_SESSION['user_id'] . ",
				name = '" . str_replace("'", "\'", $data['name']) . "',
				attachment_file_name = '" . str_replace("'", "\'", $data['attachment']) . "'
			WHERE
				id = " . str_replace("'", "\'", $data['product_type_id']) . ";";
		} else {
			$sql =
			"INSERT INTO `product_type`
			(
				`created_by`,  
				`attachment_file_name`,
				`name`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", $data['attachment']) . "', 
				'" . str_replace("'", "\'", $data['name']) . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `product_type` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}