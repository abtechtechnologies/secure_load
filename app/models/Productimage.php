<?php
class Productimage extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM product_image WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM product_image WHERE deleted = 0;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getProductImages($product_id)
	{
		$sql = 'SELECT * FROM product_image WHERE deleted = 0 AND product_id = ' . $product_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	//@todo
	public function writeData($product_id, $attachments)
	{
		$status = 1; 
		
		if ($product_id > 0) {
			for ($count = 1; $count < 6; $count++) {
				if ($status == 1) {
					$sql =
						"UPDATE `product_image` SET
							modified_by = " . $_SESSION['user_id'] . ",
							attachment_file_name = '" . $attachments[$count] . "'
						WHERE
							product_id = " . $product_id . " AND
							count = " . $count . ";";
				}
					
				$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
			}
		} else {
			for ($count = 1; $count < 6; $count++) {
				if ($status == 1) {
					$sql =
					"INSERT INTO `product_image`
				(
					`created_by`,
					`product_id`,
					`count`,
					`attachment_file_name`
				) VALUES (
					'" . $_SESSION['user_id'] . "',
					'" . str_replace("'", "\'", trim($product_id)) . "',
					'" . $count . "',
					'" . $attachments[$count] . "'
				);";
				}

				$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
			}
		}
		
		return $status;
	}
	
	public function writeSingleData($product_id, $attachment, $count)
	{
		if ($product_id > 0) {
			$sql =
			"UPDATE `product_image` SET
					modified_by = " . $_SESSION['user_id'] . ",
					attachment_file_name = '" . $attachment . "'
				WHERE
					product_id = " . $product_id . " AND
					count = " . $count . ";";
			
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		} else {
			$sql =
			"INSERT INTO `product_image`
			(
				`created_by`,
				`product_id`,
				`count`,
				`attachment_file_name`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . str_replace("'", "\'", trim($product_id)) . "',
				'" . $count . "',
				'" . str_replace("'", "\'", trim($attachment)) . "'
			);";
			
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
		
		return $status;
	}
	
	public function updateDB($product_id)
	{
		$status = 1;
	
		for ($count = 1; $count < 6; $count++) {
			if ($status == 1) {
				$sql =
				"INSERT INTO `product_image`
				(
					`created_by`,
					`product_id`,
					`count`,
					`attachment_file_name`
				) VALUES (
					'" . $_SESSION['user_id'] . "',
					'" . str_replace("'", "\'", trim($product_id)) . "',
					'" . $count . "',
					''
				);";
			}
				
			$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		}
	
		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `product` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}