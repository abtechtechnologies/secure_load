<?php
class Expense extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM expense WHERE id = ' . $id . ' AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
		
		return $return;
	}
	
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM expense WHERE deleted = 0 ORDER BY id;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM expense WHERE 1;';
	
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecentexpenseId()
	{
		$sql = 'SELECT * FROM expense WHERE deleted = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getexpensesByexpenseType($expense_type_id)
	{
		$sql = 'SELECT * FROM expense WHERE deleted = 0 AND parent_id = 0 AND expense_type_id = ' . $expense_type_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getexpensesByParentId($expense_id)
	{
		$sql = 'SELECT * FROM expense WHERE deleted = 0 AND parent_id = ' . $expense_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getexpensesByTransactionId($expense_id)
	{
		$sql = 'SELECT * FROM sale WHERE deleted = 0 AND expense_id = ' . $expense_id . ';';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[] = $row['expense_id'];
		}
	
		return $return;
	}
	
	public function getSizes($id)
	{
		$sql = 'SELECT * FROM expense WHERE parent_id = ' . $id . ' AND deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getexpenseSizes()
	{
		$sql = 'SELECT * FROM expense WHERE deleted = 0 AND parent_id > 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['parent_id']][$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByTime($hours)
	{
		$date = strtotime(date("Y-m-d H:m:s", strtotime('-' . $hours . ' hours', time())));
		
		$sql = "SELECT * FROM expense WHERE deleted = 0 ORDER BY created_date DESC;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if ($row['alt_date'] == '0000-00-00 00:00:00') {
				if (strtotime($row['created_date']) > $date) {
					$return[$row['id']] = $row;
				}
			} else {
				if (strtotime($row['alt_date']) > $date) {
					$return[$row['id']] = $row;
				}
			}
			
		}
		
		return $return;
	}
	
	public function getRecordsByDate($date)
	{
		$sql = "SELECT * FROM expense WHERE deleted = 0 ORDER BY created_date DESC;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$row_date = substr($row['created_date'], 0, 10);
			
			if ($date == $row_date) {
				$return[$row['id']] = $row;
			}
		}
		
		return $return;
	}
	
	public function getRecordsByDateRange($date1, $date2)
	{
		$sql = "SELECT * FROM expense WHERE deleted = 0 ORDER BY id DESC;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			if ($row['alt_date'] == '0000-00-00 00:00:00') {
				
				$row_date = explode(' ', $row['created_date']);
				$row_date = $row_date[0];
			} else {
				$row_date = explode(' ', $row['alt_date']);
				$row_date = $row_date[0];
			}
			
			if ($date1 == $date2) {
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['id']] = $row;
				}
			} else {
				
				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
	}
	
	public function getRecordsByDateTimeRange($date1, $date2)
	{
		$sql = "SELECT * FROM expense WHERE deleted = 0 ORDER BY id DESC;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			if ($row['alt_date'] == '0000-00-00 00:00:00') {
				
				$row_datetime = $row['created_date'];
			} else {
				$row_datetime = $row['alt_date'];
			}
			
			$time1 = strtotime($date1);
			$time2 = strtotime($date2);
			
			$row_time = strtotime($row_datetime);
			
			if ($time1 >= $row_time && $row_time >= $time2) {
				$return[$row['id']] = $row;
			}
		}
		
		return $return;
	}
	
	public function writeData($id, $title, $amount, $attachment_file_name, $reoccurring, $reoccurring_start, $alt_date, $note, $left_owed, $vendor_id, $product_id)
	{
		if ($id > 0) {
			$sql =
			"UPDATE `expense` SET
				modified_by = " . $_SESSION['user_id'] . ",
				title = '" . str_replace("'", "\'", trim($title)) . "',
				amount = '" . str_replace("'", "\'", trim($amount)) . "',
				attachment_file_name = '" . str_replace("'", "\'", trim($attachment_file_name)) . "',
				reoccurring = '" . str_replace("'", "\'", trim($reoccurring)) . "',
				reoccurring_start = '" . str_replace("'", "\'", trim($reoccurring_start)) . "',
				alt_date = '" . str_replace("'", "\'", trim($alt_date)) . "',
				left_owed = '" . str_replace("'", "\'", trim($left_owed)) . "',
				vendor_id = '" . str_replace("'", "\'", trim($vendor_id)) . "',
				product_id = '" . str_replace("'", "\'", trim($product_id)) . "',
				note = '" . str_replace("'", "\'", trim($note)) . "'
			WHERE
				id = " . str_replace("'", "\'", $id) . ";";
		} else {
			$sql =
			"INSERT INTO `expense`
			(
				`created_by`, 
				`title`, 
				`amount`, 
				`attachment_file_name`, 
				`reoccurring`, 
				`reoccurring_start`, 
				`alt_date`, 
				`left_owed`,
				`vendor_id`,
				`product_id`, 
				`note`
			) VALUES (
				'" . $_SESSION['user_id'] . "', 
				'" . str_replace("'", "\'", trim($title)) . "', 
				'" . str_replace("'", "\'", trim($amount)) . "', 
				'" . str_replace("'", "\'", trim($attachment_file_name)) . "',
				'" . str_replace("'", "\'", trim($reoccurring)) . "', 
				'" . str_replace("'", "\'", trim($reoccurring_start)) . "', 
				'" . str_replace("'", "\'", trim($alt_date)) . "', 
				'" . str_replace("'", "\'", trim($left_owed)) . "', 
				'" . str_replace("'", "\'", trim($vendor_id)) . "', 
				'" . str_replace("'", "\'", trim($product_id)) . "', 
				'" . str_replace("'", "\'", trim($note)) . "'
			);";
		}
		
		$status =  $this->DB->query(preg_replace( '/\s+/', ' ', $sql));

		return $status;
	}
	
	public function delete($id)
	{
		$sql = 'UPDATE `expense` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}