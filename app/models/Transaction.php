<?php
class Transaction extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		
		$this->load->model('client');
		$client = $this->client->getRecord($_SESSION['client_id']);
		
		$this->DB = $this->load->database($client['db_name'], TRUE);
	}
	
	public function getMostRecent()
	{
		$sql = 'SELECT * FROM transaction WHERE deleted = 0 AND order_delivered = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row['id'];
		}
	
		return $return;
	}
	
	public function getLastTransactionByUser($user_id)
	{
		$sql = 'SELECT * FROM transaction WHERE user_id = ' . $user_id . ' AND deleted = 0 AND order_delivered = 0 ORDER BY id DESC LIMIT 1;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
	
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getRecord($id)
	{
		$sql = 'SELECT * FROM transaction WHERE id = ' . $id . ' AND deleted = 0 AND order_delivered = 0 ;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = false;
		
		foreach ($db->result_array() as $row) {
			$return = $row;
		}
	
		return $return;
	}
	
	public function getScheduled()
	{
		$sql = 'SELECT * FROM transaction WHERE deleted = 0 AND schedule_flag = 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecords()
	{				
		$sql = 'SELECT * FROM transaction WHERE deleted = 0;';

		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
				
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getPending()
	{
		$sql = 'SELECT * FROM transaction WHERE pending = 1 AND deleted = 0;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAdmin()
	{
		$sql = 'SELECT * FROM transaction WHERE 1;';
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
		
		return $return;
	}
	
	public function getRecordsAlt()
	{
	$sql = 'SELECT * FROM transaction WHERE deleted = 0;';
	
	$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	$return = array();
	
	foreach ($db->result_array() as $row) {
	$return[$row['id']] = $row;
	}
	
	return $return;
	}
	
	public function getRecordsByUserId($id)
	{
		$sql = 'SELECT * FROM transaction WHERE user_id = ' . $id . ' AND deleted = 0 AND order_delivered = 0 ;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getDriverOrders($id)
	{
		$sql = 'SELECT * FROM transaction WHERE order_delivered = ' . $id . ' AND deleted = 0;';
	
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		$return = array();
	
		foreach ($db->result_array() as $row) {
			$return[$row['id']] = $row;
		}
	
		return $return;
	}
	
	public function getRecordsByTime($hours)
	{
		$date = strtotime(date("Y-m-d H:m:s", strtotime('-' . $hours . ' hours', time())));
		
		$sql = "SELECT * FROM transaction WHERE deleted = 0 ORDER BY created_date DESC;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			if ($row['changed_date'] == '0000-00-00 00:00:00') {
				
				$row_created_date = strtotime($row['created_date']);
				
				//FOR DAYLIGHT SAVINGS
				$row_created_date = $row_created_date - 3600;
				
				if ($row_created_date > $date) {
					$return[$row['id']] = $row;
				}
			} else {
				if (strtotime($row['changed_date']) > $date) {
					$return[$row['id']] = $row;
				}
			}
			
		}
		
		return $return;
	}
	
	public function getRecordsByDate($date)
	{
		$sql = "SELECT * FROM transaction WHERE deleted = 0 ORDER BY created_date DESC;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			$row_date = substr($row['created_date'], 0, 10);
			
			if ($date == $row_date) {
				$return[$row['id']] = $row;
			}
		}
		
		return $return;
	}
	
	public function getRecordsByDateRange($date1, $date2)
	{
		$sql = "SELECT * FROM transaction WHERE deleted = 0 ORDER BY id DESC;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			if ($row['changed_date'] == '0000-00-00 00:00:00') {
				
				$row_date = explode(' ', $row['created_date']);
				$row_date = $row_date[0];
			} else {
				$row_date = explode(' ', $row['changed_date']);
				$row_date = $row_date[0];
			}
			
			if ($date1 == $date2) {
				//1 Date
				if ($date1 == $row_date) {
					$return[$row['id']] = $row;
				}
			} else {

				//Date range
				$time1 = strtotime($date1);
				$time2 = strtotime($date2);
				
				$row_time = strtotime($row_date);
				
				//FOR DAYLIGHT SAVINGS
				$row_time = $row_time - 3600;
				
				if ($time1 <= $row_time && $row_time <= $time2) {
					$return[$row['id']] = $row;
				}
			}
		}
		
		return $return;
	}
	
	public function getRecordsByDateTimeRange($date1, $date2)
	{
		$sql = "SELECT * FROM transaction WHERE deleted = 0 ORDER BY id DESC;";
		
		$db = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		$return = array();
		
		foreach ($db->result_array() as $row) {
			
			if ($row['changed_date'] == '0000-00-00 00:00:00') {
				
				$row_datetime = $row['created_date'];
			} else {
				$row_datetime = $row['changed_date'];
			}
			
			$time1 = strtotime($date1);
			$time2 = strtotime($date2);
			
			$row_time = strtotime($row_datetime);
			
			//FOR DAYLIGHT SAVINGS
			$row_time = $row_time - 3600;

			if ($time1 >= $row_time && $row_time >= $time2) {
				$return[$row['id']] = $row;
			}
		}

		return $return;
	}
	
	public function writeData($cost, $user_id, $notes, $alt_cost = 0, $subtotal = 0, $new_created_date = '', $cashcard = '', $new_created_time = '', $credit_merchant_id = 1, $pending = 0, $sale_by = 0)
	{

		if (count(explode('/', $new_created_date)) > 1) {
			$dates = explode('/', $new_created_date);
			
			$new_date = $dates[2] . '-' . $dates[0] . '-' . $dates[1] . ' ' . $new_created_time . ':00';
		} else {
			if ($new_created_date == '') {
				$new_date = '0000-00-00 00:00:00';
			} else {
				$new_date = $new_created_date . ' ' . $new_created_time . ':01';
			}
		}
		
		if ($cashcard == 1) {
			$cashcard = 'cash';
		}
		
		if ($cashcard == 2) {
			$cashcard = 'card';
		}

		if (trim($new_created_date) == '') {
			$sql =
			"INSERT INTO `transaction`
			(
				`created_by`,
				`user_id`,
				`notes`,
				`payment_type`,
				`alt_cost`,
				`sub_total`,
				`credit_merchant_id`,
				`pending`,
				`sale_by`,
				`cost`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . $user_id . "',
				'" . $notes . "',
				'" . $cashcard . "',
				'" . $alt_cost . "',
				'" . $subtotal . "',
				'" . $credit_merchant_id . "',
				'" . $pending . "',
				'" . $sale_by . "',
				'" . $cost . "'
			);";
		} else {
			$sql =
			"INSERT INTO `transaction`
			(
				`changed_date`,
				`created_by`,
				`user_id`,
				`notes`,
				`payment_type`,
				`alt_cost`,
				`sub_total`,
				`credit_merchant_id`,
				`pending`,
				`sale_by`,
				`cost`
			) VALUES (
				'" . $new_date . "',
				'" . $_SESSION['user_id'] . "',
				'" . $user_id . "',
				'" . $notes . "',
				'" . $cashcard . "',
				'" . $alt_cost . "',
				'" . $subtotal . "',
				'" . $credit_merchant_id . "',
				'" . $pending . "',
				'" . $sale_by . "',
				'" . $cost . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function updateData($data, $credit_merchant_id = 1)
	{
		
		$sql =
		"UPDATE `transaction` SET
			modified_by = " . $_SESSION['user_id'] . ",
			changed = 1,
			changed_date = '" . $data['time'] . "',
			notes = '" . $data['notes'] . "',
			credit_merchant_id = '" . $credit_merchant_id . "',
			alt_cost = '" . str_replace("'", "\'", trim($data['alt_cost'])) . "'
		WHERE
			id = " . str_replace("'", "\'", $data['transaction_id']) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function updateSubTotal($id, $amount)
	{
		
		$sql =
		"UPDATE `transaction` SET
			sub_total = '" . str_replace("'", "\'", trim($amount)) . "'
		WHERE
			id = " . str_replace("'", "\'", $id) . ";";
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function writeDataSchedule($time, $date, $cost, $user_id, $notes, $alt_cost = 0, $subtotal = 0, $new_created_date = '', $cashcard = '', $credit_merchant_id = 1, $pending = 0, $sale_by = 0)
	{
		if ($cashcard == 1) {
			$cashcard = 'cash';
		}
		
		if ($cashcard == 2) {
			$cashcard = 'card';
		}
		
		if ($new_created_date == '') {
			$sql =
			"INSERT INTO `transaction`
			(
				`created_by`,
				`user_id`,
				`notes`,
				`payment_type`,
				`alt_cost`,
				`sub_total`,
				`schedule_date`,
				`schedule_time`,
				`schedule_flag`,
				`credit_merchant_id`,
				`pending`,
				`sale_by`,
				`cost`
			) VALUES (
				'" . $_SESSION['user_id'] . "',
				'" . $user_id . "',
				'" . $notes . "',
				'" . $cashcard . "',
				'" . $alt_cost . "',
				'" . $subtotal . "',
				'" . $date . "',
				'" . $time . "',
				1,
				'" . $credit_merchant_id . "',
				'" . $pending . "',
				'" . $sale_by . "',
				'" . $cost . "'
			);";
		} else {
			$sql =
			"INSERT INTO `transaction`
			(
				`created_date`,
				`created_by`,
				`user_id`,
				`notes`,
				`payment_type`,
				`alt_cost`,
				`sub_total`,
				`schedule_date`,
				`schedule_time`,
				`schedule_flag`,
				`credit_merchant_id`,
				`pending`,
				`sale_by`,
				`cost`
			) VALUES (
				'" . $new_created_date . " 01:00:00',
				'" . $_SESSION['user_id'] . "',
				'" . $user_id . "',
				'" . $notes . "',
				'" . $cashcard . "',
				'" . $alt_cost . "',
				'" . $subtotal . "',
				'" . $date . "',
				'" . $time . "',
				1,
				'" . $credit_merchant_id . "',
				'" . $pending . "',
				'" . $sale_by . "',
				'" . $cost . "'
			);";
		}
		
		return $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	}
	
	public function delete($id)
	{
		$this->load->model('sale');
		$sales = $this->sale->getRecordsByTransactionId($id);
		
		$sql = 'UPDATE `transaction` SET deleted = 1 WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		if ($status == 1) {
			foreach ($sales as $sale_id => $data) {
				$status = $this->sale->delete($sale_id);
			}
		}
	
		return $status;
	}
	
	public function changePending($id)
	{
		$this->load->model('transaction');
		$trans = $this->transaction->getRecord($id);
		
		if ($trans['pending'] == 1) {
			$pending = 0;
		} else {
			$pending = 1;
		}
		
		$sql = 'UPDATE `transaction` SET pending = ' . $pending . ' WHERE id = ' . $id;
		
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
		
		return $status;
	}
	
	public function acceptOrder($id)
	{
		$sql = 'UPDATE `transaction` SET order_accepted = ' . $_SESSION['admin_user_id'] . ' WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}

	public function cashOut($id)
	{
	$sql = 'UPDATE `transaction` SET cashed_out = 1 WHERE id = ' . $id;
	
	$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
	return $status;
	}
	
	public function completeOrder($id)
	{
		$sql = 'UPDATE `transaction` SET order_delivered = ' . $_SESSION['admin_user_id'] . ' WHERE id = ' . $id;
	
		$status = $this->DB->query(preg_replace( '/\s+/', ' ', $sql));
	
		return $status;
	}
}