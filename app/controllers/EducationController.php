<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EducationController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{		
		$this->load->model('education');
		$this->page_data['educations'] = $this->education->getRecords();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function view()
	{
		$this->load->model('education');
		$this->page_data['education'] = $this->education->getRecord($this->input->get('id'));
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
}