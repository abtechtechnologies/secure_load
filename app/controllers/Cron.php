<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {
	
	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$status = 1;

		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$this->load->model('user');
		$this->load->model('product');
		$this->load->model('productsize');
		$this->load->model('sale');
		$this->load->model('transaction');
		$sales = $this->transaction->getScheduled();
		
		//Format list by sales person
		$alt_sales = array();
		
		foreach ($sales as $id => $data) {
			if (date('F j') == $data['schedule_date']) {
				$alt_sales[$id] = $data;
				$alt_sales[$id]['user'] = $this->user->getRecord($data['user_id']);
				$alt_sales[$id]['sales'] = $this->sale->getRecordsByTransactionId($id);
				
				foreach ($alt_sales[$id]['sales'] as $sale_id => $data2) {
					$alt_sales[$id]['sales'][$sale_id]['product'] = $this->product->getRecord($data2['product_id']);
					$alt_sales[$id]['sales'][$sale_id]['product_size'] = $this->productsize->getRecord($data2['product_size_id']);
				}
			}
		}
		
		$this->load->library('email');
		$this->email->set_mailtype("html");
		
		$this->email->from('reporter@wwweed.org', 'WWWeed Reporter');
		
		//$dev = true;
		$dev = false;
		
		$this->email->to('apogeereports@gmail.com');
		
		if ($dev == true) {
			$recip = array(
				'ahartjoe@gmail.com',
				'psilver82@gmail.com',
				'stoodley.ben@gmail.com'
			);
		} else {
			$recip = array(
				'ahartjoe@gmail.com',
				'psilver82@gmail.com',
				'stoodley.ben@gmail.com'
			);
		}
		
		$this->email->bcc($recip);
		
		$this->email->subject("Last Night's Scheduled Sales - " . date("m/d/Y"));
		
		$data = array(
			'date' => date("m/d/Y"),
			'site' => $company['site'],
			'company' => $company['name'],
			'logo' => $company['logo_light'],
			'logo_text' => $company['logo_dark'],
			'sales' => $alt_sales,
			'sales_count' => count($alt_sales)
		);
		
		$body = $this->load->view('email/morning_report.phtml', $data, TRUE);
		$this->email->message($body);
		
		$this->email->send();
		
		$status = $this->email->print_debugger();
		
		if (trim(strip_tags($status)) == 0) {
			$status = 1;
		}
		
		print($status);
		exit;
	}
}
