<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CartController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{		

		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
}