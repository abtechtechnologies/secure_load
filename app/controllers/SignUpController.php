<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SignUpController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}

	public function index()
	{
		$page['page'] = 'sign-up/index';
		
		$page['member'] = $this->input->get('member');
		
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
	
	public function member()
	{
		$page['page'] = 'sign-up/member';
		$page['error'] = false;
		
		$this->load->model('companyinfo');
		$company = $this->companyinfo->getRecord();
		
		$page['key'] = $this->input->get('key', false);
		$page['user_id'] = $this->input->get('user_id', false);
		
		if ($page['key'] != false && $page['user_id'] != false) {
			$this->load->model('maillink');
			$status = $this->maillink->unlock($page['user_id'], $page['key']);
			
			if ($status != 1) {
				redirect('http://' . $company['site']);
			}
		} else {
			redirect('http://' . $company['site']);
		}
		
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
}