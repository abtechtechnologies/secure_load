<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function sitemap()
	{
		$this->load->view('home/urllist.txt');
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();

		$this->load->model('companyinfo');
		$page['company_info'] = $this->companyinfo->getRecord();
		
		$this->load->model('gripe');
		$page['gripes'] = $this->gripe->getRecords();
		
		$page['in_use_gripes'] = array();
		$page['not_in_use_gripes'] = array();
		
		foreach ($page['gripes'] as $id => $data) {
			if ($data['in_use'] == 1 || $data['current_load'] != '') {
				$page['in_use_gripes'][$id] = $data;
			} else {
				$page['not_in_use_gripes'][$id] = $data;
			}
		}

		$this->load->view('/home/index.phtml', $page);
	}
	
	public function getGripesAction()
	{
		$this->load->model('_preloader');
		$page['init'] = $this->_preloader->load();
		
		$this->load->model('gripe');
		$page['gripes'] = $this->gripe->getRecords();
		
		$page['in_use_gripes'] = array();
		$page['not_in_use_gripes'] = array();
		
		$page['low_battery'] = array();
		$page['not_low_battery'] = array();
		
		foreach ($page['gripes'] as $id => $data) {
			if ($data['in_use'] == 1 || $data['current_load'] != '') {
				$page['in_use_gripes'][$id] = $data;
			} else {
				$page['not_in_use_gripes'][$id] = $data;
			}
			
			if ($data['battery_level'] < 40) {
				$page['low_battery'][$id] = $data;
			} else {
				$page['not_low_battery'][$id] = $data;
			}
		}
		
		$array = array(
			'gripes' => $page['gripes'],
			'in_use_gripes' => $page['in_use_gripes'],
			'not_in_use_gripes' => $page['not_in_use_gripes'],
			'low_battery' => $page['low_battery'],
			'not_low_battery' => $page['not_low_battery']
		);
		
		print_r(json_encode($array));
		exit;
	}
	
	public function test()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Set # of visits to home page views for now
		$this->load->model('pagecount');
		$status = $this->pagecount->increaseCount();
		
		$this->page_data['signup'] = $this->input->get('signup');
		
		$this->load->model('producttype');
		$this->page_data['product_types'] = $this->producttype->getRecords(true);
		
		$this->load->model('product');
		$this->page_data['sub_products'] = $this->product->getProductSizes();
		
		foreach ($this->page_data['product_types'] as $id => $data) {
			$products = $this->product->getProductsByProductType($id);
			
			foreach ($products as $id2 => $data2) {
				$this->page_data['products'][$id][$id2] = $data2;
			}
		}
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function location()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/location';
		
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
	
	public function updateSingleTensionAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		$this->load->model('gripe');
		$status = $this->gripe->updateSingleTension($_POST['gripe_id'], $_POST['current_tension'], $_POST['load']);
		
		print $status;
		exit;
	}
	
	public function SetMultipleLoadAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$gripes = $_POST['gripe_ids'];
		$load = $_POST['load'];
		
		$this->load->model('gripe');
		
		foreach ($gripes as $i => $gripe_id) {
			$status = $this->gripe->setLoad($gripe_id, $load);
		}
		
		print $status;
		exit;
	}
	
	public function saleTest()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'home/sale-test';
		
		 require 'lib/paytrace/vendor/autoload.php';
		
		$client = new GuzzleHttp\Client();
		
		// DRY
		$auth_server = 'https://api.paytrace.com';
		
		// send a request to the authentication server
		// note: normally, you'd store the username/password in a more secure fashion!
		$res = $client->post($auth_server . "/oauth/token", [
			'body' => [
				'grant_type' => 'password', 
				'username' => 'JoeAhart', 
				'password' => 'Tranzq123!'
			]
		]);
		
		// grab the results (as JSON)
		$json = $res->json();
		
		// get the access token
		$token = $json['access_token'];
		
		// create a fake sale transaction
		$sale_data = array(
			"amount" => 19.99, 
			"credit_card" => array(
				"number" => "4111111111111111",
				"expiration_month" => "12",
				"expiration_year" => "2020"
			),
			"billing_address" => array(
				"name" => "Steve Smith", 
				"street_address" => "8320 E. West St.",
				"city" => "Spokane",
				"state" => "WA", 
				"zip" => "85284"
			)
		);
		
		try {
			// post the transaction to hermes
			$res = $client->post($auth_server . "/v1/transactions/sale/keyed", [
				'headers' => [  'Authorization' => "Bearer " . $token],
				'json' => $sale_data
			]);
		} catch (Exception $e) {
			//@TODO
				print_r('!!! - ' . $e->getMessage());
				exit;
		}
		
		$response = $res->json();
		
		// dump the json results
		$result = var_export($res->json());
			
		//@TODO
			print '<pre>';
			print_r($result);
			print '</pre>';
			exit;
	}

}