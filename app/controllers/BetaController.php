<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BetaController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}

	public function index()
	{
		$page['page'] = 'beta/index';
				
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);
	}
}