<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StoreController extends CI_Controller {

	protected $page_data = '';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}

	public function index()
	{		
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('product');
		$this->page_data['product'] = $this->product->getRecord($this->input->get('product_id'));
		
		$this->load->model('productsize');
		$this->page_data['product_sizes'] = $this->productsize->getRecordsByProductID($this->input->get('product_id'));

		$this->page_data['lowest_quantity'] = 999;
		
		foreach($this->page_data['product_sizes'] as $product_id => $data) {
			if ($data['units'] < $this->page_data['lowest_quantity']) {
				$this->page_data['lowest_quantity'] = $data['units'];
			}
		}

		$this->load->model('user');
		$this->page_data['users'] = $this->user->getUsers();
		
		$this->load->model('review');
		$this->page_data['reviews'] = $this->review->getRecordsByProductId($this->input->get('product_id'));

		$this->load->model('producttype');
		$this->page_data['product_types'] = $this->producttype->getRecords(true);
		
		$this->load->model('grower');
		$this->page_data['growers'] = $this->grower->getRecords();
		
		$this->load->model('productimage');
		$this->page_data['product_images'] = $this->productimage->getProductImages($this->input->get('product_id'));
		
		foreach ($this->page_data['product_images'] as $id => $data) {
			if ($data['attachment_file_name'] == '') {
				unset($this->page_data['product_images'][$id]);
			}
		}
		
		$product_type_id = $this->page_data['product']['product_type_id'];
		$related_products = $this->product->getProductsByProductType($product_type_id);
		
		unset($related_products[$this->input->get('product_id')]);

		shuffle($related_products);
		
		$this->page_data['related_products'] = array();
		
		$count = 0;
		foreach ($related_products as $i => $data) {
		if ($count < 6) {

		$this->page_data['related_products'][$data['id']] = $related_products[$i];
		}
			
		$count++;
		}


		$this->load->model('inventory');
		$this->page_data['inventory'] = $this->inventory->getProductInventory($this->input->get('product_id'));
		
		foreach ($this->page_data['product_sizes'] as $id => $data) {
			if ($data['name'] == '') {
				unset($this->page_data['product_sizes'][$id]);
			}
		}
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);	
	}
	
	public function addToCart()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$status = 1;
		
		$this->load->model('product');
		$this->load->model('productsize');
		$product_size = $this->productsize->getRecord($_POST['product_size_id']);
		$product = $this->product->getRecord($product_size['product_id']);
				
		if ($_POST['user_id'] == 0) {
			//Save to temp cart
			if (!isset($_SESSION['temp_cart'])) {
				$_SESSION['temp_cart'] = array();
			}
			
			if (!isset($_SESSION['temp_cart'][$product['id']])) {
				$_SESSION['temp_cart'][$product['id']] = array();
			}
			
			$_SESSION['temp_cart'][$product['id']] = $product;
			$_SESSION['temp_cart'][$product['id']]['product_size'] = $product_size;
			$_SESSION['temp_cart'][$product['id']]['product_size_id'] = $product_size['id'];
			$_SESSION['temp_cart'][$product['id']]['quantity'] = 1;
		} else {
			//Save to database cart
			$this->load->model('cart');
			$status = $this->cart->writeData($_POST['user_id'], $product, $product_size);
		}
		
		print $status;
	}
	
	public function increaseQuantity()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$status = 1;
		
		$this->load->model('cart');
		$status = $this->cart->increaseQuantity($_POST['cart_id']);

		print $status;
	}
	
	public function decreaseQuantity()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$status = 1;
		
		$this->load->model('cart');
		$status = $this->cart->decreaseQuantity($_POST['cart_id']);
		
		print $status;
	}
	
	public function addReview()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
	$data = array(
	'user_id' => $this->input->post('user_id'),
	'product_id' => $this->input->post('product_id'),
	'rating' => $this->input->post('rating'),
	'description' => $this->input->post('description')
	);
	
	$this->load->model('review');
	$status = $this->review->writeData($data);
	
	print $status;
	exit;
	}
	
	public function purchaseAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->database();
		$this->db->trans_start();
		
		$this->load->model('companyinfo');
		$page_data['company_info'] = $this->companyinfo->getRecord();
		
		if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
			//Don't allow access without a logged in user
			redirect('http://' . $page_data['company_info']['site']);
		}
		
		$this->load->model('sale');
		$this->load->model('user');
		$this->load->model('cart');
		$this->load->model('product');
		$this->load->model('productsize');
		$this->load->model('transaction');
		$this->load->model('inventory');
		
		$drivers = $this->user->getDrivers();
		
		$driver_emails = array();
		
		foreach ($drivers as$id => $data) {
			$driver_emails[$id] = $data['email'];
		}

		$status = 1;
		
		//Record Transaction
		if ($status == 1) {
			$status = $this->transaction->writeData($_POST['cash_due'], $_SESSION['user_id'], str_replace("'", "\'", $_POST['notes']), 0, $_POST['subtotal'], '', $_POST['payment_type']);
			
			$transaction_id = $this->transaction->getMostRecent();
		}

		$products_string = array();
		
		foreach ($_POST['cart'] as $cart_id => $data) {
			
			$product = $this->product->getRecord($data['product_id']);
			$product_size = $this->productsize->getRecord($data['product_size_id']);
			$products_string[] = $product['name'] . " (" . $product_size['name'] . " x " . $data['quantity'] . ")";

			if ($status == 1) {
				if ($data['product']['inventory_holder_id'] > 0) {
					$holder_id = $data['product']['inventory_holder_id'];
				} else {
					$holder_id = $page_data['company_info']['primary_inventory_user_id'];
				}
				
				$sale_array = array(
					'sale_id' => 0,
					'transaction_id' => $transaction_id,
					'user_id' => $_POST['user_id'],
					'product_id' => $data['product']['id'],
					'product_size_id' => $data['product_size_id'],
					'quantity' => $data['quantity'],
					'holder_user_id' => $holder_id,
					'new_created_date' => ''
				);
				
				$status = $this->sale->writeData($sale_array);
				
				//DONT REMOVE INVENTORY IF ITS A TEST
				if ($_POST['user_id'] != 2) {
					$product_size = $this->productsize->getRecord($data['product_size_id']);
					
					//Remove stock from inventory
					if ($status == 1) {
						
						$company = $this->companyinfo->getRecord();
						
						$product = $this->product->getRecord($product_size['product_id']);
						
						if ($product['inventory_holder_id'] > 0) {
							$user_id = $product['inventory_holder_id'];
						} else {
							$user_id = $company['primary_inventory_user_id'];
						}
						
						$status = $this->inventory->removeInventory($product_size['product_id'], $product_size['units'] * $data['quantity'], $user_id);
					}
				}
			} else {
				break;
			}
		}
		
		//Remove balance from account
		if ($status == 1) {
			$status = $this->user->updateBalance($_POST['user_id'], $_POST['subtotal']);
		}
		
		$customer = $this->user->getRecord($_POST['user_id']);
		
		//DONT ALERT DRIVERS IF ITS A TEST
		if ($_POST['user_id'] == 2) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			
			$this->email->from('info@' . $page_data['company_info']['site_show'], $page_data['company_info']['name'] . ' Admin');
			
			$this->email->to('ahartjoe@gmail.com');
			
			$this->email->subject('Order Available For Delivery From ' . $page_data['company_info']['name'] . '!');
			
			$mail_data = array(
				'company' => $page_data['company_info'],
				'company_name' => $page_data['company_info']['name'],
				'name' => $customer['first_name'],
				'phone' => $customer['phone'],
				'customer' => $customer['first_name'] . ' ' . $customer['last_name'],
				'delivery_address' => $customer['address'] . ', ' . $customer['city'] . ', ' . $customer['zip'],
				'site' => $page_data['company_info']['site'],
				'products' => $products_string,
				'notes' => $_POST['notes']
			);
			
			$body = $this->load->view('email/delivery-available.phtml', $mail_data, TRUE);
			$this->email->message($body);
			
			$this->email->send();
			
			$status = $this->email->print_debugger();
			
			if (trim(strip_tags($status)) == 0) {
				$status = 1;
			}
			
			$this->email->clear(true);
			
		} else {
			//Notify drivers
			$drivers = $this->user->getDrivers();
			
			foreach ($drivers as $id => $data) {
				if ($status == 1) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					
					$this->email->from('info@' . $page_data['company_info']['site_show'], $page_data['company_info']['name'] . ' Admin');
					
					if ($page_data['company_info']['dev_mode'] == 1) {
						$this->email->to('apogeewebdesign@gmail.com');
					} else {
						$this->email->to($data['email']);
					}
					
					$this->email->subject('Order Available For Delivery From ' . $page_data['company_info']['name'] . '!');
					
					$mail_data = array(
						'company' => $page_data['company_info'],
						'company_name' => $page_data['company_info']['name'],
						'name' => $customer['first_name'],
						'phone' => $customer['phone'],
						'customer' => $customer['first_name'] . ' ' . $customer['last_name'],
						'delivery_address' => $customer['address'] . ', ' . $customer['city'] . ', ' . $customer['zip'],
						'site' => $page_data['company_info']['site'],
						'products' => $products_string,
						'notes' => $_POST['notes']
					);
					
					$body = $this->load->view('email/delivery-available.phtml', $mail_data, TRUE);
					$this->email->message($body);
					
					$this->email->send();
					
					$status = $this->email->print_debugger();
					
					if (trim(strip_tags($status)) == 0) {
						$status = 1;
					}
					
					$this->email->clear(true);
				}
			}
		}
		
		
		//Empty cart
		if ($status == 1) {
			$status = $this->cart->emptyCart($_POST['user_id']);
		}
		
		//Empty temp cart
		if ($status == 1) {
			$_SESSION['temp_cart'] = array();
		}
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			$status = 'DB transaction error';
		} else {
			//Send customer invoice
			$user = $this->user->getRecord($_POST['user_id']);
			
			$this->load->library('email');
			$this->email->set_mailtype("html");
			
			$this->email->from('info@' . $page_data['company_info']['site_show'], $page_data['company_info']['name'] . ' Admin');
			
			if ($page_data['company_info']['dev_mode'] == 1) {
				$this->email->to('apogeewebdesign@gmail.com');
			} else {
				$this->email->to($user['email']);
			}
			
			//$this->email->cc('another@another-example.com');
			//$this->email->bcc('them@their-example.com');
			
			$this->email->subject('Your Reciept From ' . $page_data['company_info']['name'] . '.');
			
			$total = 0;
			
			foreach ($_POST['cart'] as $cart_id => $data) {
				if (is_numeric($cart_id)) {
					$product_size = $this->productsize->getRecord($data['product_size_id']);
					
					$total += $product_size['price'] * $data['quantity'];
					
					$_POST['cart'][$cart_id]['product'] = $this->product->getRecord($data['product_id']);
					$_POST['cart'][$cart_id]['product_size'] = $this->productsize->getRecord($data['product_size_id']);
				} else {
					unset($_POST['cart'][$cart_id]);
				}
			}
			
			$data = array(
				'name' => $user['first_name'] . ' ' . $user['last_name'],
				'transaction_id' => $transaction_id,
				'customer' => $user['first_name'] . ' ' . $user['last_name'],
				'delivery_address1' => $user['address'],
				'delivery_address2' => 	$user['city'] . ', ' . $user['zip'],
				'site' => $page_data['company_info']['site'],
				'phone' => $user['phone'],
				'notes' => $_POST['notes'],
				'total' => $total,
				'products' => $_POST['cart'],
				'schedule_date' => '',
				'schedule_time' => '',
				'normal' => true
			);

			$body = $this->load->view('email/invoice.phtml', $data, TRUE);
			$this->email->message($body);
			
			$this->email->send();
			
			$status = $this->email->print_debugger();
			
			if (trim(strip_tags($status)) == 0) {
				$status = 1;
			} 
		}
		
		if ($status == 1 && $_SESSION['user_id'] != 2) {
			if ($page_data['company_info']['delivery'] == 2) {
				$this->load->model('onfleet');
				$status = $this->onfleet->placeOrder($user['address'], $user['city'], 'CA', $user['first_name'], $user['last_name'], $user['phone'], $_POST['notes'], $_POST['cart'], $_POST['cash_due'], $order_string);
			}
		}
		
		print $status;
	}
	
	public function schedulePurchaseAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->database();
		$this->db->trans_start();
		
		$this->load->model('companyinfo');
		$company_info = $this->companyinfo->getRecord();
		
		$source = $this->input->post('source');
		
		if (!isset($_SESSION['user_id']) || $_SESSION['user_id'] == 0) {
			//Don't allow access without a logged in user
			redirect('http://' . $company_info['site']);
		}
		
		if ($source == 2) {
			$cart = $_POST['cart']['cart_records'];
		} else {
			$cart = $_POST['cart'];
		}
		
		$this->load->model('sale');
		$this->load->model('user');
		$this->load->model('cart');
		$this->load->model('product');
		$this->load->model('productsize');
		$this->load->model('transaction');
		$this->load->model('inventory');
		
		$products = $this->product->getRecords();
		$product_sizes = $this->productsize->getRecords();
		
		$drivers = $this->user->getDrivers();
		
		$driver_emails = array();
		
		foreach ($drivers as$id => $data) {
			$driver_emails[$id] = $data['email'];
		}
		
		$sub_total = 0;

		foreach ($cart as $cart_id => $data) {
			$sub_total += $product_sizes[$data['product_size_id']]['price'] * $data['quantity'];
		}
		
		$status = 1;
		
		//Record Transaction
		if ($status == 1) {
			$status = $this->transaction->writeDataSchedule($_POST['schedule_time'], $_POST['schedule_date'], $_POST['cash_due'], $_POST['user_id'], $_POST['notes'], $_POST['cash_due'], $sub_total, '', $_POST['payment_type'], 1, $_POST['pending'], $_POST['sale_by']);
															
			$transaction_id = $this->transaction->getMostRecent();
		}
		
		$order_string = '\n';
		$product_array = array();
		
		$count = 1;
				
		foreach ($cart as $cart_id => $data) {

			if ($status == 1) {
				$sale_array = array(
					'sale_id' => 0,
					'transaction_id' => $transaction_id,
					'user_id' => $_POST['user_id'],
					'product_id' => $data['product_id'],
					'product_size_id' => $data['product_size_id'],
					'quantity' => $data['quantity'],
					'holder_user_id' => $data['holder_user_id'],
					'new_created_date' => ''
				);
				
				$status = $this->sale->writeData($sale_array);
				
				$order_string .= '- ' . $products[$data['product_id']]['name'] . ' (' . $product_sizes[$data['product_size_id']]['name'] . ' x ' . $data['quantity'] . ')';
				
				if ($count < count($_POST['cart']['cart_records'])) {
					$order_string .= '\n';
				}
				
				$product_array[] = $products[$data['product_id']]['name'] . ' (' . $product_sizes[$data['product_size_id']]['name'] . ' x ' . $data['quantity'] . ')';
				
				$count++;
			} else {
				break;
			}
		}
		
		//Remove balance from account
		if ($status == 1) {
			$status = $this->user->updateBalance($_POST['user_id'], $_POST['subtotal']);
		}
		
		//Empty cart
		if ($status == 1) {
			$status = $this->cart->emptyCart($_POST['user_id']);
		}
		
		//Empty temp cart
		if ($status == 1) {
			$_SESSION['temp_cart'] = array();
		}
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			$status = 'DB transaction error';
		} else {
			if ($company_info['general_user_id'] != $_POST['user_id']) {
				//Send customer invoice. general user is not a customer
				$user = $this->user->getRecord($_POST['user_id']);
				
				$this->load->library('email');
				$this->email->set_mailtype("html");
				
				$this->email->from('info@' . $company_info['site_show'], $company_info['name'] . ' Admin');
				
				if ($company_info['dev_mode'] == 1) {
					$this->email->to('apogeewebdesign@gmail.com');
				} else {
					$this->email->to($user['email']);
				}
				
				//$this->email->cc('another@another-example.com');
				//$this->email->bcc('them@their-example.com');
				
				if ($_POST['pending'] == 1) {
					$this->email->subject('Your PENDING Invoice From ' . $company_info['name'] . '.');
					
					$pending = 1;
					$schedule = 0;
				} else {
					$this->email->subject('Your SCHEDULED Invoice From ' . $company_info['name'] . '.');
					
					$pending = 0;
					$schedule = 1;
				}
				
				$total = 0;
				
				foreach ($cart as $cart_id => $data) {
					$product_size = $this->productsize->getRecord($data['product_size_id']);
					$total += $product_size['price'] * $data['quantity'];
				}
				
				$data = array(
					'name' => $user['first_name'] . ' ' . $user['last_name'],
					'transaction_id' => $transaction_id,
					'customer' => $user['first_name'] . ' ' . $user['last_name'],
					'delivery_address1' => $user['address'],
					'delivery_address2' => 	$user['city'] . ', ' . $user['zip'],
					'site' => $company_info['site'],
					'company' => $company_info,
					'phone' => $user['phone'],
					'notes' => $_POST['notes'],
					'total' => $total,
					'products' => $_POST['cart'],
					'schedule_date' => $_POST['schedule_date'],
					'schedule_time' => $_POST['schedule_time'],
					'schedule' => $schedule,
					'pending' => $pending
				);
				
				$body = $this->load->view('email/invoice.phtml', $data, TRUE);
				$this->email->message($body);
				
				$this->email->send();
				
				$status = $this->email->print_debugger();
				
				if (trim(strip_tags($status)) == 0) {
					$status = 1;
				}
			}
		}
		
		
		$this->load->library('email');
		$this->email->set_mailtype("html");
		
		$this->email->from('info@' . $company_info['site_show'], $company_info['name'] . ' Admin');
		
		$this->email->to('ahartjoe@gmail.com');
		
		if ($_POST['pending'] == 1) {
			$this->email->subject('PENDING Order Made ' . $company_info['name']);
			
			$pending = 1;
			$schedule = 0;
		} else {
			$this->email->subject('SCHEDULED Order Made - ' . $company_info['name']);
			
			$pending = 0;
			$schedule = 1;
		}
		
		$this->email->subject('(DEV) Order SCHEDULED - ' . $company_info['name']);
		
		$mail_data = array(
			'company' => $company_info,
			'company_name' => $company_info['name'],
			'name' => 'JOE (ADMIN)',
			'phone' => $user['phone'],
			'customer' => $user['first_name'] . ' ' . $user['last_name'],
			'delivery_address' => $user['address'] . ', ' . $user['city'] . ', ' . $user['zip'],
			'site' => $company_info['site'],
			'products' => $product_array,
			'notes' => $_POST['notes'],
			'schedule_date' => $_POST['schedule_date'],
			'schedule_time' => $_POST['schedule_time'],
			'schedule' => $schedule,
			'pending' => $pending
		);
		
		$body = $this->load->view('email/scheduled-delivery-available.phtml', $mail_data, TRUE);
		$this->email->message($body);
		
		$this->email->send();
		
		$status = $this->email->print_debugger();
		
		if (trim(strip_tags($status)) == 0) {
			$status = 1;
		}
		
		print $status;
	}
	
	public function purchaseAdminAction()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();

		//Start transaction
		$this->load->database();
		$this->db->trans_start();
		
		$this->load->model('companyinfo');
		$page_data['company_info'] = $this->companyinfo->getRecord();
		
		if (!isset($_SESSION['admin_user_id']) || $_SESSION['admin_user_id'] == 0) {
			//Don't allow access without a logged in user
			redirect('http://' . $page_data['company_info']['site']);
		}
		
		$this->load->model('sale');
		$this->load->model('user');
		$this->load->model('cart');
		$this->load->model('product');
		$this->load->model('productpart');
		$this->load->model('productsize');
		$this->load->model('transaction');
		$this->load->model('inventory');

		$products = $this->product->getRecords(true);
		$product_sizes = $this->productsize->getRecords();
		
		$status = 1;
		
		$sub_total = 0;

		foreach ($_POST['cart']['cart_records'] as $cart_id => $data) {
			if (isset($data['id'])) {
				$sub_total += $data['cost'];
			}
		}

		//@todo only 1 credit merchant currently
		if ($_POST['cashcard'] == 'cash') {
			$credit_merchant_id = 0;
		} else {
			$credit_merchant_id = 1;
		}
		
		//Record Transaction
		if ($status == 1) {
			$status = $this->transaction->writeData(
				$_POST['cash_due'], 
				$_POST['user_id'], 
				str_replace("'", "\'", $_POST['notes']), 
				$_POST['alt_due'], 
				$sub_total, 
				$_POST['new_created_date'], 
				$_POST['cashcard'], 
				$_POST['new_created_time'],
				$credit_merchant_id,
				$_POST['pending'],
				$_POST['sale_by']
			);
			
			$transaction_id = $this->transaction->getMostRecent();
		}
		
	//	$carts = $this->cart->getRecordsByUserId($_POST['user_id']);
	
		//Piece together string for telling what was ordered
		$products_list = array();

		foreach ($_POST['cart']['cart_records'] as $cart_id => $data) {
			if (isset($data['id'])) {
				
				$product = $this->product->getRecord($data['product_id']);
				$product_size = $this->productsize->getRecord($data['product_size_id']);
				
				$products_string[] = $product['name'] . " (" . $product_size['name'] . " x " . $data['quantity'] . ")";
				
				if ($status == 1) {
					$sale_array = array(
						'sale_id' => 0,
						'transaction_id' => $transaction_id,
						'user_id' => $_POST['user_id'],
						'product_id' => $data['product_id'],
						'product_size_id' => $data['product_size_id'],
						'quantity' => $data['quantity'],
						'holder_user_id' => $data['holder_id'],
						'new_created_date' => $_POST['new_created_date']
					);
					
					$status = $this->sale->writeData($sale_array);
					
					//DONT REMOVE INVENTORY IF ITS A TEST
					//if ($_POST['user_id'] != 2) {
					//Remove stock from inventory
					if ($status == 1) {
						if ($product['combo'] == 1) {
							$parts = $this->productpart->getRecordsByProductId($data['product_id']);

							foreach ($parts as $product_part_id => $product_part_record) {
								$inventory = $this->inventory->getRecordsByProductId($product_part_record['product_id']);
								
								$product = $this->product->getRecord($product_part_record['product_id']);
								
								$inventory_id_set = 0;
								$quantity_start = 0;
								
								foreach ($inventory as $inventory_id => $inventory_record) {
									if ($inventory_record['user_id'] == $data['holder_id']) {
										$inventory_id_set = $inventory_id;
										$quantity_start = $inventory_record['quantity'];
									}
								}
						
								$new_stock = $quantity_start - $product_part_record['product_units'] * $data['quantity'];
								
								$status = $this->inventory->writeData($inventory_id_set, $new_stock, 0, 0, $product['unit_name']);

							}
						} else {
							$status = $this->inventory->removeInventory($data['product_size_id'], $data['quantity'], $data['holder_id']);
						}
					}
				//	}
				} else {
					break;
				}
			}
		}

		//Remove balance from account
		if ($status == 1) {
			$status = $this->user->updateBalanceAdmin($_POST['user_id'], $_POST['new_balance']);
		}
		
		$customer = $this->user->getRecord($_POST['user_id']);
		
		
		$count = 1;

		
		//DONT ALERT DRIVERS IF ITS A TEST
		if ($_POST['user_id'] == $page_data['company_info']['dev_id'] || $_SESSION['admin_user_id'] == $page_data['company_info']['dev_id']) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			
			$this->email->from('info@' . $page_data['company_info']['site_show'], $page_data['company_info']['name'] . ' Admin');
			
			$this->email->to('ahartjoe@gmail.com');
			
			$this->email->subject('(DEV) Order Available For Delivery - ' . $page_data['company_info']['name'] . '!');
			
			$mail_data = array(
				'company' => $page_data['company_info'],
				'company_name' => $page_data['company_info']['name'],
				'name' => 'JOE (ADMIN)',
				'phone' => $customer['phone'],
				'customer' => $customer['first_name'] . ' ' . $customer['last_name'],
				'delivery_address' => $customer['address'] . ', ' . $customer['city'] . ', ' . $customer['zip'],
				'site' => $page_data['company_info']['site'],
				'products' => $products_string,
				'notes' => $_POST['notes']
			);
			
			$body = $this->load->view('email/delivery-available.phtml', $mail_data, TRUE);
			$this->email->message($body);
			
			$this->email->send();
			
			$status = $this->email->print_debugger();
			
			if (trim(strip_tags($status)) == 0) {
				$status = 1;
			}
			
			$this->email->clear(true);
			
		} else {
			//Notify drivers
			$drivers = $this->user->getDrivers();
			
			foreach ($drivers as $id => $data) {
				if ($status == 1) {
					$this->load->library('email');
					$this->email->set_mailtype("html");
					
					$this->email->from('info@' . $page_data['company_info']['site_show'], $page_data['company_info']['name'] . ' Admin');
					
					if ($page_data['company_info']['dev_mode'] == 1) {
						$this->email->to('apogeewebdesign@gmail.com');
					} else {
						$this->email->to($data['email']);
					}
					
					$this->email->subject('Order Completed - ' . $page_data['company_info']['name']);
					
					$mail_data = array(
						'company' => $page_data['company_info'],
						'company_name' => $page_data['company_info']['name'],
						'name' => $data['first_name'],
						'phone' => $customer['phone'],
						'customer' => $customer['first_name'] . ' ' . $customer['last_name'],
						'delivery_address' => $customer['address'] . ', ' . $customer['city'] . ', ' . $customer['zip'],
						'site' => $page_data['company_info']['site'],
						'products' => $products_string,
						'notes' => $_POST['notes']
					);
					
					$body = $this->load->view('email/delivery-available.phtml', $mail_data, TRUE);
					$this->email->message($body);
					
					$this->email->send();
					
					$status = $this->email->print_debugger();
					
					if (trim(strip_tags($status)) == 0) {
						$status = 1;
					}
					
					$this->email->clear(true);
				}
			}
		}
		
		//Empty cart
		if ($status == 1) {
			$status = $this->cart->emptyCart($_POST['user_id']);
		}
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			$status = 'DB transaction error';
		} else {
			//Send customer invoice
			$user = $this->user->getRecord($_POST['user_id']);
			
			$this->load->library('email');
			$this->email->set_mailtype("html");
			
			$this->email->from('info@' . $page_data['company_info']['site_show'], $page_data['company_info']['name'] . ' Admin');
			
			if ($page_data['company_info']['dev_mode'] == 1) {
				$this->email->to('apogeewebdesign@gmail.com');
			} else {
				$this->email->to($user['email']);
			}
			
			$this->email->subject('Your Receipt From ' . $page_data['company_info']['name'] . '.');
			
			$total = 0;
			
			foreach ($_POST['cart'] as $cart_id => $data) {
				if (is_numeric($cart_id)) {
					$product_size = $this->productsize->getRecord($data['product_size_id']);
					
					$total += $product_size['price'] * $data['quantity'];
					
					$_POST['cart'][$cart_id]['product'] = $this->product->getRecord($data['product_id']);
					$_POST['cart'][$cart_id]['product_size'] = $this->productsize->getRecord($data['product_size_id']);
				} else {
					unset($_POST['cart'][$cart_id]);
				}
			}
			
			if ($_POST['alt_due'] > 0) {
				$discount = (1 - round($_POST['alt_due'] / $sub_total, 2)) * 100;
			} else {
				$discount = 0;
			}
			
			$data = array(
				'name' => $user['first_name'] . ' ' . $user['last_name'],
				'transaction_id' => $transaction_id,
				'transaction' => $this->transaction->getRecord($transaction_id),
				'discount' => $discount,
				'customer' => $user['first_name'] . ' ' . $user['last_name'],
				'delivery_address1' => $user['address'],
				'delivery_address2' => 	$user['city'] . ', ' . $user['zip'],
				'site' => $page_data['company_info']['site'],
				'phone' => $user['phone'],
				'notes' => $_POST['notes'],
				'total' => $total,
				'products' => $_POST['cart'],
				'schedule_date' => '',
				'schedule_time' => ''
			);
			
			$body = $this->load->view('email/invoice_admin.phtml', $data, TRUE);
			$this->email->message($body);
			
			$this->email->send();
			
			$status = $this->email->print_debugger();
			
			if (trim(strip_tags($status)) == 0) {
				$status = 1;
			}
		}
		
		if ($status == 1 && $_SESSION['admin_user_id'] != 2) {
			if ($page_data['company_info']['delivery'] == 2) {
				$this->load->model('onfleet');
				$status = $this->onfleet->placeOrder($user['address'], $user['city'], 'CA', $user['first_name'], $user['last_name'], $user['phone'], $_POST['notes'], $_POST['cart'], $_POST['cash_due'], $order_string);
			}
		}
		
		//Send email to drivers
		
		print $status;
		exit;
	}
	
	public function dataMerge()
	{	
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);
	}
	
	public function cart()
	{	
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('productsize');
		$this->page_data['product_sizes'] = $this->productsize->getRecords();
		
		$this->load->model('product');
		$this->page_data['products'] = $this->product->getRecords(true);
		$products = $this->product->getRecords(true);
		
		$all_products = $this->product->getRecords(true);
		
		$this->load->model('user');
		
		if (isset($_SESSION['user_id'])) {
			$this->page_data['user'] = $this->user->getRecord($_SESSION['user_id']);
			
			//Strip extra chars and format phone number
			$this->page_data['user']['phone'] = str_replace(')', '', str_replace('(', '', str_replace('.', '', str_replace('-', '', $this->page_data['user']['phone']))));
			$this->page_data['user']['phone'] = substr_replace($this->page_data['user']['phone'], '-', 3, 0);
			$this->page_data['user']['phone'] = substr_replace($this->page_data['user']['phone'], '-', 7, 0);

		}

		$this->page_data['time'] = date ('H', time());
		
		$this->page_data['day_times'] = array(
			'7:00 AM' => '7:00 AM',	
			'7:30 AM' => '7:30 AM',	
			'8:00 AM' => '8:00 AM',	
			'8:30 AM' => '8:30 AM',	
			'9:00 AM' => '9:00 AM',	
			'9:30 AM' => '9:30 AM',	
			'10:00 AM' => '10:00 AM',	
			'10:30 AM' => '10:30 AM',	
			'11:00 AM' => '11:00 AM',	
			'11:30 AM' => '11:30 AM',	
			'12:00 PM' => '12:00 PM',	
			'12:30 PM' => '12:30 PM',	
			'1:00 PM' => '1:00 PM',	
			'1:30 PM' => '1:30 PM',	
			'2:00 PM' => '2:00 PM',	
			'2:30 PM' => '2:30 PM',	
			'3:00 PM' => '3:00 PM',	
			'3:30 PM' => '3:30 PM',	
			'4:00 PM' => '4:00 PM',	
			'4:30 PM' => '4:30 PM',	
			'5:00 PM' => '5:00 PM',	
			'5:30 PM' => '5:30 PM',	
			'6:00 PM' => '6:00 PM',	
			'6:30 PM' => '6:30 PM',	
			'7:00 PM' => '7:00 PM',	
			'7:30 PM' => '7:30 PM',	
			'8:00 PM' => '8:00 PM',	
			'8:30 PM' => '8:30 PM'
		);
		
		$deals = array();
		$this->page_data['deals'] = array();

		foreach ($this->page_data['product_sizes'] as $id => $data) {
			if ($data['price'] < 21 && $data['price'] != 0) {
				if (isset($this->page_data['products'][$data['product_id']])) {
					$deals[$id] = $this->page_data['products'][$data['product_id']];
					
				}
			}
		}

		shuffle($deals);
		
		$count = 0;
		foreach ($deals as $id => $data) {
			if ($count < 6) {
				$this->page_data['deals'][$id] = $data;
			}
			
			$count++;
		}
	
		//Init functions and page load
		$this->load->model('_loader');
		$this->_loader->load($this->page_data);	
	}
	
	public function checkout()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'store/checkout';
	
		$this->load->view('_header/store.phtml', $page);
		$this->load->view($page['page'] . '.phtml', $page);
		$this->load->view('_footer/store.phtml', $page);	}
	
	public function shop()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'store/shop';
	
		$this->load->view('_header/store.phtml', $page);
		$this->load->view($page['page'] . '.phtml', $page);
		$this->load->view('_footer/store.phtml', $page);	}
	
	public function singleProduct()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$page['page'] = 'store/single-product';
		
		$this->load->model('product');
		$page['product'] = $this->product->getRecord($this->input->get('product_id'));
		
		$this->load->model('producttype');
		$page['product_types'] = $this->producttype->getRecords(true);
		
		//Init functions and page load
		$this->load->model('_loader');
		$page['loader'] = $this->_loader->load($page);	
	}

	public function removeCartItem()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$this->load->model('cart');
		$status = $this->cart->delete($this->input->post('cart_id'));
	
		print $status;
	}
	
	public function removeTempCartItem()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		foreach ($_SESSION['temp_cart'] as $product_id => $data) {
			if ($product_id == $_POST['product_id']) {
				unset($_SESSION['temp_cart'][$product_id]);
			}
		}
	
		print 1;
	}
	
	public function adminAddToCart()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$status = 1;
		
		$this->load->model('product');
		$this->load->model('productsize');
		$product_size = $this->productsize->getRecord($_POST['product_size_id']);
		$product = $this->product->getRecord($product_size['product_id']);
		
		if ($_POST['user_id'] == 0) {
			//Save to temp cart
			if (!isset($_SESSION['temp_cart'])) {
				$_SESSION['temp_cart'] = array();
			}
			
			if (!isset($_SESSION['temp_cart'][$product['id']])) {
				$_SESSION['temp_cart'][$product['id']] = array();
			}
			
			$_SESSION['temp_cart'][$product['id']] = $product;
			$_SESSION['temp_cart'][$product['id']]['product_size'] = $product_size;
			$_SESSION['temp_cart'][$product['id']]['product_size_id'] = $product_size['id'];
			$_SESSION['temp_cart'][$product['id']]['quantity'] = 1;
		} else {
			//Save to database cart
			$this->load->model('cart');
			$status = $this->cart->writeData($_POST['user_id'], $product, $product_size);
		}
		
		$cart_id = $this->cart->getRecentId();
		
		print $cart_id;
	}
	
	public function adminCreateCart()
	{
		$this->load->model('_preloader');
		$this->page_data['init'] = $this->_preloader->load();
		
		$status = 1;
		
		$this->load->model('product');
		$this->load->model('productsize');
		$product_size = $this->productsize->getRecord($_POST['product_size_id']);
		$product = $this->product->getRecord($product_size['product_id']);
		
		if ($_POST['user_id'] == 0) {
			//Save to temp cart
			if (!isset($_SESSION['temp_cart'])) {
				$_SESSION['temp_cart'] = array();
			}
			
			if (!isset($_SESSION['temp_cart'][$product['id']])) {
				$_SESSION['temp_cart'][$product['id']] = array();
			}
			
			$_SESSION['temp_cart'][$product['id']] = $product;
			$_SESSION['temp_cart'][$product['id']]['product_size'] = $product_size;
			$_SESSION['temp_cart'][$product['id']]['product_size_id'] = $product_size['id'];
			$_SESSION['temp_cart'][$product['id']]['quantity'] = 1;
		} else {
			//Save to database cart
			$this->load->model('cart');
			$status = $this->cart->writeData($_POST['user_id'], $product, $product_size);
		}
		
		$cart_id = $this->cart->getRecentId();
		
		print $cart_id;
	}
	

}